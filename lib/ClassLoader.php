<?php

/**
 * 类加载器
 */
class ClassLoader {

    /**
     * 
     * @global type $config
     * @param type $className
     * @param type $c
     * @return boolean
     */
    public static function load($className, $c = false) {
        global $config;
        $bLoad = false;
        foreach ($config->classRoot as $_classRoot) {
            $_path = $_classRoot . $className . ".php";
            if (strpos($className, 'Smarty_') !== false) {
                $className = strtolower($className);
            }
            if (is_file($_path)) {
                include_once $_path;
                $c && $c->$className = new $className();
                $bLoad = true;
                break;
            }
        }
        if (!$bLoad && $config->debug) {
            #die('Class file' . $_path . 'not found!<br>');
        } else {
            return true;
        }
        unset($bLoad);
    }

}

spl_autoload_register(array('ClassLoader', 'load'));
