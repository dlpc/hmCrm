<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class vAlbum extends Controller {

    public function v($Q) {
        $id = $Q->id;
        if ($id > 0) {
            $this->cacheId = $id;
            $this->loadModel('Album');
            $album = $this->Album->getAlbum($id);
            $this->Smarty->assign('gpath', $album['gpath']);
            $this->Smarty->assign('gname', $album['gname']);
            $this->show();
        }
    }

    public function ls($Q) {
        $cid = intval($Q->id);
        if ($cid > 0) {
            $this->cacheId = $cid;
            $this->loadModel('Album');
            $albums = $this->Album->getAlbums($cid);
            // group by date
            $albs = array();
            foreach ($albums as $alb) {
                if (!is_array($albs[$alb['date']])) {
                    $albs[$alb['date']] = array();
                }
                $albs[$alb['date']][] = $alb;
            }
            #var_dump($albs);
            $this->Smarty->assign('albums', $albs);
            $this->show();
        }
    }

}
