<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxIdeas extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        $this->loadModel('Ideas');
        parent::__construct($ControllerName, $Action, $QueryString);
    }

    public function get() {
        $cid = $this->pPost('cid');
        $this->Smarty->assign('ideas', $this->Ideas->gets($cid));
        $this->show('./profile_ideas.tpl');
    }

    public function set() {
        $cid = $this->pPost('cid');
        $cont = $this->pPost('cont');
        echo $this->Dao->insert(DBPREFIX . 'ideas', 'cid, idea, `date`')->values(array($cid, $cont, 'NOW()'))->exec();
    }

}
