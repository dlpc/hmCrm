<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Home extends Controller {

    public function Index() {
        if ($this->pCookie('logintoken')) {
            // home pass
        } else {
            $this->redirect('?/Login');
        }
        $this->show();
    }

    public function welcome(){
        $this->show();
    }
    
    public function customer($Q) {
        $this->loadModel('Customer');
        $this->loadModel('mJournal');
        $this->loadModel('CustomerBaby');
        $this->cacheId = $Q->page . $Q->pagesize;
        $list = $this->Customer->getCustomerList($Q->page, $Q->pagesize);
        foreach ($list as &$l) {
            $l['journal'] = $this->mJournal->getUserJournalList($l['id']);
            $l['babys'] = $this->CustomerBaby->getCustomerBabys($l['id']);
            foreach ($l['journal'] as &$j) {
                $j['jdate'] = $this->Util->dateTimeFormat($j['jdate']);
            }
        }
        $this->Smarty->assign('list', $list);
        $this->show();
    }

}
