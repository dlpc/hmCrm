<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxCustomer extends Controller {

    /**
     * @param type $ControllerName
     * @param type $Action
     * @param type $QueryString
     */
    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('Customer');
    }

    /**
     * ajax获取用户信息
     * @param type $cid
     * @return Json JsonArray
     */
    public function ajaxGetCustomerInfo($cid) {
        if ($this->isDec($cid)) {
            $ret = $this->Customer->getCustomerInfo($cid);
            $this->echoJson(array('s' => 1, 'data' => $ret));
        } else {
            $this->echoJson(array('s' => 0));
        }
    }

    /**
     * 获取客户生日列表
     * @param type $Q
     * @return Json JsonArray
     */
    public function ajaxGetCustomerBirthList($Q) {
        $Q->limit = isset($Q->limit) ? intval($Q->limit) : 20;
        $ret = $this->Customer->getCustomerBirthList($Q->limit);
        $this->echoJson($ret);
    }

    /**
     * ajax 删除客户
     */
    public function ajaxDeleteCustomer() {
        $id = $this->pPost('id');
        if ($this->isDec($id)) {
            echo $this->Dao->delete()->from(DBPREFIX . 'customers')->where('id=' . $id)->exec();
        } else {
            echo 0;
        }
    }

    public function ajaxUpdateHead() {
        $this->loadModel('ImageUploader');
        $this->ImageUploader->dir = dirname(__FILE__) . "/../upload/";

        $fileName = $this->ImageUploader->upload();

        if ($fileName) {
            $this->echoJson(array(
                's' => 0,
                'imgn' => $fileName
            ));
        } else {
            $this->echoJson(array(
                's' => 0,
            ));
        }
    }

    /**
     * Ajax确认用户头像
     */
    public function ajaxUpdateConfirmHead() {
        $cid = $this->pPost('cid');
        $head = $this->post('head');
        $ret = $this->Dao->update(DBPREFIX . 'customers')->set(array(
                    'cus_head' => $head
                ))->where('id=' . $cid)->exec();
        echo $ret ? 1 : 0;
    }

    /**
     * Ajax确认宝宝头像
     */
    public function ajaxUpdateConfirmBabyHead() {
        $id = $this->pPost('bid');
        $head = $this->post('head');
        $ret = $this->Dao->update(DBPREFIX . 'babys')->set(array(
                    'b_head' => $head
                ))->where('id=' . $id)->exec();
        echo $ret ? 1 : 0;
    }

    public function GetCustomerInfo($Q) {
        $cid = $Q->cid;
        $this->loadModel('Customer');
        $this->cacheId = uniqid();
        $this->Smarty->caching = false;
        $cus = $this->Customer->getCustomerInfo($cid);
        $this->Smarty->assign('cus', $cus);
        $this->show();
    }

    public function ajaxAlterCustomer() {
        $cid = $this->pPost('cid');
        if ($cid > 0) {
            $set = array();
            $gid = false;
            $data = $this->post('data');
            foreach ($data as &$d) {
                $set[] = "`$d[name]` = '$d[value]'";
            }
            $set = implode(',', $set);
            $sql = "UPDATE " . DBPREFIX . 'customers' . " SET $set WHERE `id` = $cid";
            echo $this->Db->query($sql);
        } else {
            // add
            $field = array();
            $values = array();
            $data = $this->post('data');
            $data[] = array('name' => 'cus_uid', 'value' => $this->pCookie('uid'));
            foreach ($data as &$d) {
                $field[] = "`$d[name]`";
                $values[] = "'$d[value]'";
            }
            $SQL = sprintf("INSERT INTO `hmcrm_customers` (%s) VALUES (%s);", implode(',', $field), implode(',', $values));
            $ret = $this->Db->query($SQL);
            echo $ret ? $ret : 0;
        }
    }

    /**
     * 搜索客户资料
     * @param type $Q
     */
    public function search($Q) {
        $this->cacheId = $Q->key;
        $key = addslashes(urldecode($Q->key));
        $ret = $this->Customer->searchCustomer($key);
        $this->Smarty->assign('ret', $ret);
        $this->show();
    }

    /**
     * ajax编辑客户资料
     * @param type $Q
     */
    public function editProfile($Q) {
        $id = intval($Q->id);
        if ($id > 0) {
            $ret = $this->Customer->getCustomerInfo($id);
            $this->Smarty->assign('cus', $ret);
            $this->Smarty->assign('ed', true);
        } else {
            $this->Smarty->assign('ed', false);
        }
        $this->show();
    }
    
    public function getIdList(){
        $list = $this->Dao->select('id')->from(DBPREFIX . 'customers')->orderby('id')->desc()->exec();
        $ls = array();
        foreach($list as $l){
            $ls[] = intval($l['id']);
        }
        $this->echoJson($ls);
    }

}
