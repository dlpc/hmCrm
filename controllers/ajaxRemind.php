<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxRemind extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        $this->loadModel('Remind');
        parent::__construct($ControllerName, $Action, $QueryString);
    }

    public function get() {
        $cid = $this->pPost('cid');
        $this->Smarty->assign('reminds', $this->Remind->gets($cid));
        $this->show('./profile_remind.tpl');
    }

    public function set() {
        $cid = $this->pPost('cid');
        $cont = $this->pPost('cont');
        echo $this->Dao->insert(DBPREFIX . 'remind', 'cid, remind, `date`')->values(array($cid, $cont, 'NOW()'))->exec();
    }

}
