<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxConsult extends Controller {

    public function updateHead() {
        $id = $this->getHid();
        $head = $this->post('head');
        $this->Dao->update(DBPREFIX . 'consultant')->set(array('chead' => $head))->where('id=' . $id)->exec();
    }

    public function vProfile() {
        $id = $this->getHid();
        $this->cacheId = $id;
        $this->show();
    }

    public function add() {
        $this->show();
    }

    public function ajaxAdd() {

        $this->loadModel('Secure');

        $head = 'layouts/images/profle_1.png';
        $pwd = 'hm2015';

        $email = trim($this->post('email'));
        $cname = trim($this->post('name'));

        $pwdh = $this->Secure->enCrypt($email, $pwd);

        echo $this->Dao->insert(DBPREFIX . 'consultant', 'email,cname,chead,csex,cphone,pwd')->values(array($email, $cname, $head, $this->post('sex'), $this->post('phone'), $pwdh))->exec();
    }

}
