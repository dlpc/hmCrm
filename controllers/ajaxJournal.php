<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxJournal extends Controller {

    /**
     * ajax获取日志内容
     * @param int $cid
     * @param int $page
     * @param int $pagesize
     */
    public function ajaxGetJournalList($Q) {

        $this->loadModel('mJournal');

        $cid = $Q->cid;
        $this->Smarty->caching = false;
        $this->cacheId = $cid;

        if ($this->isDec($cid)) {
            $res = $this->mJournal->getUserJournalList($cid);
            foreach ($res as &$j) {
                $j['jdate'] = $this->Util->dateTimeFormat($j['jdate']);
            }
            $this->Smarty->assign('journal', $res);
            $this->show();
        } else {
            echo 0;
        }
    }

    /**
     * ajax删除Journal
     * @param int id
     */
    public function ajaxDeleteJournal() {
        $id = $this->pPost('id');
        if ($this->isDec($id)) {
            $this->loadModel('mJournal');
            echo $this->mJournal->deleteJournal($id);
        } else {
            echo 0;
        }
    }

    public function ajaxAlterJournal() {
        $this->loadModel('Feeds');
        $uid = $this->pCookie('uid');
        $cid = $this->pPost('cid');
        $content = $this->pPost('content');
        $this->loadModel('mJournal');
        $id = $this->pPost('id');
        if ($id > 0) {
            // alter
        } else {
            // add
            $id = $this->mJournal->addJournal(0, $uid, $cid, '', $content);
            if ($id) {
                if ($this->Feeds->add($cid, $uid, Feeds::FEED_TYPE_JOURNAL, $content, $id)) {
                    echo 1;
                } else {
                    echo 0;
                }
            } else {
                echo 0;
            }
        }
    }

}
