<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class ajaxAlbum extends Controller {

    public function __construct($ControllerName, $Action, $QueryString) {
        parent::__construct($ControllerName, $Action, $QueryString);
        $this->loadModel('Album');
    }

    public function set() {
        $this->loadModel('Feeds');
        $cid = $this->pPost('cid');
        $uid = $this->getHid();
        $path = $this->pPost('path');
        $desc = $this->pPost('desc');
        $id = $this->Album->addAlbum($cid, $desc, $path);
        if ($id) {
            if ($this->Feeds->add($cid, $uid, Feeds::FEED_TYPE_ALBUM, array($path, $desc), $id)) {
                echo $id;
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function delete() {
        
    }

}
