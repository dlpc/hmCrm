<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class ajaxBaby extends Controller {

    public function delete() {
        $bid = $this->pPost('bid');
        if ($this->isDec($bid)) {
            $this->loadModel('CustomerBaby');
            echo $this->CustomerBaby->deleteBaby($bid);
        }
    }

    /**
     * @todo
     */
    public function ajaxAlterBabyProfile() {
        $bid = intval($this->post('bid'));
        if ($bid === 0) {
            // add
            $field = array();
            $values = array();
            $data = $this->post('data');
            foreach ($data as &$d) {
                $field[] = "`$d[name]`";
                $values[] = "'$d[value]'";
            }
            $SQL = sprintf("INSERT INTO `hmcrm_babys` (%s) VALUES (%s);", implode(',', $field), implode(',', $values));
            $ret = $this->Db->query($SQL);
            echo $ret ? 1 : 0;
        } else {
            $set = array();
            $gid = false;
            $data = $this->post('data');
            foreach ($data as &$d) {
                $set[] = "`$d[name]` = '$d[value]'";
            }
            $set = implode(',', $set);
            $sql = "UPDATE " . DBPREFIX . 'babys' . " SET $set WHERE `id` = $bid";
            echo $this->Db->query($sql);
        }
    }

    /**
     * 编辑宝宝资料
     * @param type $Q
     */
    public function editBabyProfile($Q) {
        $this->Smarty->caching = false;
        $id = $Q->bid;
        if ($id > 0) {
            $this->loadModel('CustomerBaby');
            $this->Smarty->assign('b', $this->CustomerBaby->getBaby($id));
            $this->Smarty->assign('ed', true);
        } else {
            $this->Smarty->assign('ed', false);
        }
        $this->show();
    }

    /**
     * 获取宝宝列表
     * @param type $Q
     */
    public function getBabyList($Q) {
        $cid = $Q->cid;
        if ($this->isDec($cid)) {
            $this->loadModel('CustomerBaby');
            $cus = array();
            $cus['babys'] = $this->CustomerBaby->getCustomerBabys($cid);
            $this->Smarty->assign('cus', $cus);
            $this->show();
        }
    }

}
