<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Profile extends Controller {

    public function ls() {
        $this->tLogin();
        $uid = $this->getHid();
        $this->loadModel('Customer');
        $this->loadModel('mJournal');
        $this->loadModel('SystemStat');
        $this->cacheId = $uid . $this->meInfo['lev'];
        if (!$this->isCached()) {
            if ($this->meInfo['lev'] > 0) {
                $cuslist = $this->Customer->getCustomerList(0, 20, $uid);
                $newJournal = $this->mJournal->getAllJournals(5, $uid);
            } else {
                $cuslist = $this->Customer->getCustomerList();
                foreach ($cuslist as &$cu) {
                    if ($cu['cus_uid'] != $uid) {
                        $cu['uname'] = $this->Dao->select('cname')->from(DBPREFIX . 'consultant')->where("id=$cu[cus_uid]")->getOne();
                    }
                }
                $newJournal = $this->mJournal->getAllJournals();
            }
            $stat = $this->SystemStat->getSystemStat();
            foreach ($newJournal as &$j) {
                $j['jdate'] = $this->Util->dateTimeFormat($j['jdate']);
            }
            foreach ($cuslist as &$l) {
                $l['stat'] = $this->Customer->getCustomerStat($l['id']);
                $l['lastJournal'] = $this->mJournal->getUserLastJournal($l['id']);
            }
            $this->Smarty->assign('stat', $stat);
            $this->Smarty->assign('newJournal', $newJournal);
            $this->Smarty->assign('list', $cuslist);
        }
        $this->show();
    }

    public function view($Q) {
        $cid = intval($Q->id);
        $this->cacheId = $cid;
        if ($cid > 0) {

            $this->loadModel('Customer');
            $this->loadModel('mJournal');
            $this->loadModel('CustomerBaby');
            $this->loadModel('Album');
            $this->loadModel('Feeds');
            $this->loadModel('Remind');
            $this->loadModel('Ideas');

            $cus = $this->Customer->getCustomerInfo($cid);

            $cus['journal'] = $this->mJournal->getUserJournalList($cid);
            $cus['babys'] = $this->CustomerBaby->getCustomerBabys($cid);
            $cus['albums'] = $this->Album->getAlbums($cid, 3);
            $cus['feeds'] = $this->Feeds->get($cid);
            $cus['stat'] = $this->Customer->getCustomerStat($cid);

            foreach ($cus['journal'] as &$j) {
                $j['jdate'] = $this->Util->dateTimeFormat($j['jdate']);
            }

            foreach ($cus['babys'] as &$b) {
                $birth = str_replace(' ', '', $b['b_birth']);
                $y = date('Y');
                $m = date('m-d', strtotime($birth));
                $datetime1 = new DateTime($y . '-' . $m);
                $datetime2 = new DateTime(date('Y-m-d'));
                $interval = $datetime1->diff($datetime2);
                $b['birthDist'] = $interval->format('%a');
            }

            $this->Smarty->assign('cus', $cus);
            $this->Smarty->assign('journal', $cus['journal']);
            $this->Smarty->assign('reminds', $this->Remind->gets($cid));
            $this->Smarty->assign('ideas', $this->Ideas->gets($cid));
            $this->show();
        } else {
            // 
        }
    }

}
