<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Login extends Controller {

    const COOKIE_EXP = 28800;

    public function Index() {
        $this->Smarty->assign('rand', (int) rand(1, 8));
        $this->show();
    }

    /**
     * ajax登陆处理
     */
    public function ajaxLoginVerify() {

        $this->loadModel('Secure');
        $this->loadModel('Consultant');

        $email = $this->pPost('email');
        $password = $this->pPost('password');

        $realPassword = $this->Dao->select("`pwd`")->from(DBPREFIX . 'consultant')->where("`email`='" . $email . "'")->getOne();
        $uid = $this->Dao->select("`id`")->from(DBPREFIX . 'consultant')->where("`email`='" . $email . "'")->getOne();

        #cho $this->Secure->enCrypt('dev@iwshop.cn','123');

        /**
         * 密码校验
         */
        if ($this->Secure->enCrypt($email, $password) === $realPassword) {

            $ip = $this->getIp();

            $this->sCookieHttpOnly('logintoken', $this->Secure->encryptToken($ip, $email), self::COOKIE_EXP);
            $this->sCookieHttpOnly('hid', $this->Util->digEncrypt($uid), self::COOKIE_EXP);
            $this->sCookie('uid', $uid, self::COOKIE_EXP);

            $this->echoJson(array('status' => 1));
        } else {
            $this->echoJson(array('status' => 0));
        }
    }

    /**
     * ajax退出登录
     * @return boolean
     */
    public function ajaxLogout() {
        // 循环删除cookie
        foreach ($_COOKIE as $k => $v) {
            $v = null;
            setcookie($k, NULL);
        }
        header('Location:?/Login/Index/');
        return true;
    }

}
