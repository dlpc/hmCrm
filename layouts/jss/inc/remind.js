
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

define(['jquery', 'util'], function($, util) {
    var cl = {};

    /**
     * 加载remind列表
     * @param {type} cid
     * @param {type} callback
     * @returns {undefined}
     */
    cl.load = function(cid, callback) {
        $.post('?/ajaxRemind/get/', {
            cid: cid
        }, function(r) {
            callback(r);
        });
    };

    cl.onsave = function() {
        var cid = parseInt($('#cusid').val());
        $('#savebtn').unbind('click').on('click', function() {
            var cont = $('.mpdcont').val();
            $.post('?/ajaxRemind/set/', {
                cid: cid,
                cont: cont
            }, function(r) {
                if (r > 0) {
                    cl.load(cid, function(html) {
                        $('#remindList').html(html);
                    });
                    util.Alert('SUCCESS');
                    $.fancybox.close();
                } else {
                    util.Alert('FAILED', true);
                }
            });
        });
    };

    return cl;
});