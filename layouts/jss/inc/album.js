
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

define(['jquery', 'util', 'Spinner'], function($, util, Spinner) {
    var cl = {};

    cl.upload = function(f, res) {

        var cid = $(this.button[0]).attr('data-id');
        var imgsrc = res.imgn;

//        Spinner.stop();
//        $('#albumsAdd').removeClass('uploading');

        var remoteImg = new Image();
        remoteImg.src = 'upload/' + imgsrc;
        remoteImg.onload = function() {
            $('#uploadComplete').click();
            $('#albumAddFrame').find('.img').attr('src', 'upload/' + imgsrc).css('display', 'block');
            $('#albumAddComfirm').unbind('click').click(comfirmUpload);
        };

        function comfirmUpload() {
            var desc = $('#albumDesc').val();
            // [HttpPost]   
            $.post('?/ajaxAlbum/set/', {
                cid: cid,
                path: imgsrc,
                desc: desc
            }, function(r) {
                if (r > 0) {
                    if ($('.albums').length > 2) {
                        $('.albums').last().remove();
                    }
                    $('#albumList').prepend($("<a class='albums fancybox.ajax' data-fancybox-type='ajax' href='?/vAlbum/v/id=" + r + "'><img src='layouts/Thumbnail/?p=/upload/" + imgsrc + "&w=400' /></a>"));
                    fnFancyBox('.albums', util.viewAlbum);
                    util.Alert('图片上传成功');
                    $.fancybox.close();
                } else {
                    util.Alert('图片上传失败');
                }
                $('#albumDesc').val('');
            });
        }

    };

    return cl;
});