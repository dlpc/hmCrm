
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

define(['jquery'], function($) {

    var journal = {};

    /**
     * 获取日志列表
     * @param {type} page
     * @param {type} pageSize
     * @param {type} cid
     * @param {type} callback
     * @returns {undefined}
     */
    journal.getList = function(cid, callback) {
        if (cid > 0) {
            // [HttpPost]
            $.get('?/ajaxJournal/ajaxGetJournalList/cid=' + cid, function(html) {
                if (undefined !== callback) {
                    callback(html);
                }
            });
        } else {
            callback(false);
        }
    };

    /**
     * 删除日志
     * @param {type} id
     * @param {type} callback
     * @returns {undefined}
     */
    journal.delete = function(id, callback) {
        if (parseInt(id) > 0) {
            // [HttpPost]
            $.post('?/ajaxJournal/ajaxDeleteJournal/', {
                id: id
            }, function(r) {
                callback(r > 0);
            });
        } else {
            callback(false);
        }
    };

    /**
     * 添加日志
     * @param {type} jcont
     * @param {type} cid
     * @param {type} callback
     * @returns {undefined}
     */
    journal.add = function(jcont, cid, callback) {
        if (parseInt(cid) > 0) {
            // [HttpPost]
            $.post('?/ajaxJournal/ajaxAlterJournal/', {
                cid: cid,
                content: jcont
            }, function(r) {
                callback(r > 0);
            });
        } else {
            callback(false);
        }
    };

    return journal;
});