
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

var currCid = false;

requirejs(['config'], function(config) {

    require(['jquery', 'util', 'journal', 'customer', 'cardlist', 'fancyBox', 'ueditor', 'jUploader', 'Spinner', 'album', 'remind', 'ideas'], function($, util, Journal, Customer, Cardlist, fancyBox, ueditor, jUploader, Spinner, Album, Remind, Ideas) {

        if ($('.albums').length === 0) {
            $('#albumsAdd').addClass('empty');
        }

        $('#searchBox').on('keyup', function(e) {
            var currKey = 0, e = e || event;
            var searchKey = $(this).val();
            currKey = e.keyCode || e.which || e.charCode;
            if (currKey === 13) {
                if (searchKey !== '') {
                    // go search
                    $('#searchResult .ret > div').html('');
                    $('#searchResult').show();
                    Spinner.spin($('#searchResult').get(0));
                    Customer.search(searchKey, function(r) {
                        r = $(r);
                        r.find('.sCuslist a').on('click', function() {
                            $('#searchResult .close').click();
                            location.href = '?/Profile/view/id=' + $(this).attr('data-id');
                        });
                        $('#searchResult .ret > div').empty().append(r);
                        Spinner.stop();
                    });
                } else {
                    util.Alert('请输入搜索关键字', true);
                }
            }
        });

        fnFancyBox('.addjournal,.editProfile,.addProfile,.addbaby,.editbaby');
        fnFancyBox('#wIdeas', Ideas.onsave);
        fnFancyBox('#wRemind', Remind.onsave);
        fnFancyBox('#nav-addcustomer');
        fnFancyBox('#uploadComplete', function() {
            $('#albumDesc').focus();
        });

        fnFancyBox('.albums', util.viewAlbum);

        if (!config.appinit) {
            // 初次加载，赋值currCid
            currCid = parseInt($('#cusid').val());
            config.appinit = true;
            Spinner.stop();
        }

        $.jUploader({
            button: $('.jUpload').get(0),
            action: '?/ajaxCustomer/ajaxUpdateHead/',
            onComplete: Customer.updateHead
        });

        // upload Album
        $.jUploader({
            button: $('#albumsAdd').get(0),
            action: '?/ajaxCustomer/ajaxUpdateHead/',
            onComplete: Album.upload,
            onUpload: function() {
//                $('#albumsAddx').addClass('uploading');
//                Spinner.spin($('#albumsAddx').get(0));
            }
        });

        $('.jUploadBaby').each(function() {
            $.jUploader({
                button: $(this),
                action: '?/ajaxCustomer/ajaxUpdateHead/',
                onComplete: Customer.updateBabyHead
            });
        });

        /**
         * 监听添加日志Done按钮
         * @returns {undefined}
         */
        window.addJournalClick = function() {
            var content = $('.mpdcont').val();
            if (content !== '') {
                Spinner.spin($('#journal-' + currCid).get(0));
                // 调用Journal类 add函数
                Journal.add(content, currCid, function(success) {
                    if (success) {
                        util.Alert('Success');
                        Journal.getList(currCid, function(html) {
                            $('.journal').find('ul').html(html);
                            Spinner.stop();
                        });
                        $.fancybox.close();
                    } else {
                        util.Alert('System Failed', true);
                    }
                });
            } else {
                util.Alert('Please Insert Data', true);
            }
        };

        /**
         * 编辑宝宝数据
         * @param {type} bid
         * @returns {undefined}
         */
        window.editBabyProfileClick = function(bid) {
            var dt = $('#editBabyProfile').serializeArray();
            dt.push({name: 'cid', value: currCid});
            $.post('?/ajaxBaby/ajaxAlterBabyProfile/', {
                cid: currCid,
                data: dt,
                bid: bid
            }, function(r) {
                if (r > 0) {
                    if (bid > 0) {
                        util.Alert('Success');
                    } else {
                        util.Alert('Success');
                    }
                    // [HttpGet]
                    $.get('?/ajaxBaby/getBabyList/cid=' + currCid, function(html) {
                        $('.cusbabys').html(html);
                        $('.cusbabys').find('.jUploadBaby').each(function() {
                            $.jUploader({
                                button: $(this),
                                action: '?/ajaxCustomer/ajaxUpdateBabyHead/',
                                onComplete: Customer.updateBabyHead
                            });
                        });
                    });
                    $.fancybox.close();
                } else {
                    util.Alert('FAILED', true);
                }
            });
        };

        /**
         * 删除客户宝宝数据
         * @param {type} bid
         * @returns {undefined}
         */
        window.fnDeleteBaby = function(bid) {
            if (confirm('Sure to Delete?')) {
                // [HttpPost]
                $.post('?/ajaxBaby/delete/', {
                    bid: bid
                }, function(r) {
                    if (r > 0) {
                        $('#baby-' + bid).slideUp(200, function() {
                            $('#baby-' + bid).remove();
                        });
                        util.Alert('Success');
                    } else {
                        util.Alert('Failed');
                    }
                });
            }
        };

        /**
         * 删除客户信息
         * @param {type} cid
         * @returns {undefined}
         */
        window.fnDeleteCustomer = function(cid) {
            if (confirm('Sure to Delete?')) {
                // [HttpPost]
                $.post('?/ajaxCustomer/ajaxDeleteCustomer/', {
                    id: cid
                }, function(r) {
                    if (r > 0) {
                        location.href = '?/Profile/ls/';
                        util.Alert('Success');
                    } else {
                        util.Alert('Failed');
                    }
                });
            }
        };

        /**
         * 编辑资料Done按钮
         * @returns {undefined}
         */
        window.fnAlterProfile = function(cid) {
            Customer.updateInfo(cid, $('#editProfile').serializeArray(), function(result, newId) {
                if (result) {
                    if (cid > 0) {
                        Customer.getInfo(currCid, function(html) {
                            $('.profile .pl').html(html);
                        });
                    } else {
                        location.href = '?/Profile/view/id=' + newId;
                    }
                    util.Alert('Success');
                    $.fancybox.close();
                } else {
                    util.Alert('FAILED', true);
                }
            });
        };

        $('.cusProfile .profile, .Babys').hover(function() {
            $(this).find('.edit').removeClass('hidden');
        }, function() {
            $(this).find('.edit').addClass('hidden');
        });

        util.onresize(function() {

        });
    });

});