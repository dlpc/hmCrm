
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

requirejs(['config'], function (config) {
    
    require(['jquery', 'util', 'Spinner'], function ($, util, Spinner) {
        
        // password input field keyup listener
        $('#pd-form-password').keyup(function (e) {
            if (e.keyCode === 13) {
                loginCheck();
            }
        });
        
        // login btn click listener
        $('.login-gbtn').eq(0).click(loginCheck);
        
        /**
         * login check function 
         * @returns {Boolean}
         */
        function loginCheck() {
            var email = $('#pd-form-username').val();
            var pwd = $('#pd-form-password').val();
            if (email !== '' && pwd !== '') {
                $('#loginWrap').show();
                Spinner.spin($('#loginWrap').get(0));
                // [HttpPost]
                $.post('?/Login/ajaxLoginVerify/', {
                    email: email,
                    password: pwd
                }, function (r) {
                    $('#loginWrap').hide();
                    Spinner.stop();
                    if (r.status > 0) {
                        util.Alert('登录成功');
                        location.href = '?/Profile/ls/';
                    } else {
                        util.Alert('登录失败', true);
                    }
                });
            } else {
                return false;
            }
        }
        
        // resize listener
        util.onresize(function () {
            $('#login').css('margin-top', (document.documentElement.clientHeight - $('#login').height() - 70) / 2);
        });
    });
});