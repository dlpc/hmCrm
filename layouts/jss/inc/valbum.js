
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

requirejs(['config'], function(config) {

    require(['jquery', 'util', 'Spinner', 'fancyBox', 'customer'], function($, util, Spinner, fancyBox, Customer) {

        fnFancyBox('#nav-addcustomer');
        
        fnFancyBox('.photo', util.viewAlbum);

        $('#searchBox').on('keyup', function(e) {
            var currKey = 0, e = e || event;
            var searchKey = $(this).val();
            currKey = e.keyCode || e.which || e.charCode;
            if (currKey === 13) {
                if (searchKey !== '') {
                    // go search
                    $('#searchResult .ret > div').html('');
                    $('#searchResult').show();
                    Spinner.spin($('#searchResult').get(0));
                    Customer.search(searchKey, function(r) {
                        r = $(r);
                        r.find('.sCuslist a').on('click', function() {
                            $('#searchResult .close').click();
                            location.href = '?/Profile/view/id=' + $(this).attr('data-id');
                        });
                        $('#searchResult .ret > div').empty().append(r);
                        Spinner.stop();
                    });
                } else {
                    util.Alert('请输入搜索关键字', true);
                }
            }
        });

    });
});