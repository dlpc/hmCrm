/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

window.UMEDITOR_HOME_URL = 'layouts/jss/umeditor/';

require.config({
    paths: {
        util: './util',
        Spinner: '../spin.min',
        jquery: '../jquery-2.1.1.min',
        config: './config',
        customer: './customer',
        journal: './journal',
        cardlist: './cardlist',
        fancyBox: '../fancyBox/source/jquery.fancybox.pack',
        ueditor: '../umeditor/umeditor.min',
        ueditor_config: '../umeditor/umeditor.config',
        jUploader: '../jUploader.min',
        ideas: './ideas',
        remind: './remind'
    },
    shim: {
        'util': {
            exports: 'util'
        },
        'Spinner': {
            exports: 'Spinner',
            deps: ['util']
        },
        'fancyBox': {
            deps: ['jquery']
        },
        'jquery': {
            exports: '$',
            deps: ['config']
        },
        'ueditor': {
            deps: ['jquery', 'ueditor_config']
        },
        'jUploader': {
            deps: ['jquery']
        }
    },
    urlArgs: "bust=1.3.9"
    // urlArgs: "bust=" + (new Date()).getTime(),
    // xhtml: true
});

define([], function() {
    var config = {};

    /**
     * app 初始化标记
     */
    config.appinit = false;

    /**
     * app 根目录
     */
    config.shoproot = '/';
    
    /**
     * app 最终card标记
     */
    config.endOfCard = false;

    return config;
});

function log(r) {
    console.log(r);
}