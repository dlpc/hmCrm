
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

define(['jquery'], function($) {

    var customer = {};

    /**
     * 更新客户资料
     * @param {type} cid
     * @param {type} data
     * @param {type} callback
     * @returns {undefined}
     */
    customer.updateInfo = function(cid, data, callback) {
        $.post('?/ajaxCustomer/ajaxAlterCustomer/', {
            cid: cid,
            data: data
        }, function(r) {
            callback(r > 0, r);
        });
    };

    /**
     * 获取客户资料
     * @param {type} cid
     * @param {type} callback
     * @returns {undefined}
     */
    customer.getInfo = function(cid, callback) {
        if (cid > 0) {
            $.get('?/ajaxCustomer/GetCustomerInfo/cid=' + cid, function(html) {
                callback(html);
            });
        } else {
            callback(false);
        }
    };

    /**
     * ajax删除客户
     * @param {type} cid
     * @param {type} callback
     * @returns {undefined}
     */
    customer.delete = function(cid, callback) {
        cid = parseInt(cid);
        if (cid > 0) {
            // [HttpPost]
            $.post('?/ajaxJournal/ajaxDeleteCustomer/', {
                id: cid
            }, function(r) {
                callback(r > 0);
            });
        } else {
            callback(false);
        }
    };

    /**
     * 更新客户头像
     * @param {type} f
     * @param {type} res
     * @returns {undefined}
     */
    customer.updateHead = function(f, res) {
        var cid = $(this.button[0]).attr('data-id');
        var imgsrc = 'upload/' + res.imgn;
        $('.cushead').attr('src', imgsrc);
        // [HttpPost]
        $.post('?/ajaxCustomer/ajaxUpdateConfirmHead/', {
            cid: cid,
            head: imgsrc
        });
    };

    /**
     * 更新宝宝头像
     * @param {type} f
     * @param {type} res
     * @param {type} innerHtml
     * @param {type} parent
     * @returns {undefined}
     */
    customer.updateBabyHead = function(f, res) {
        var btn = $(this.button[0]);
        var imgsrc = 'upload/' + res.imgn;
        btn.find('.babyhead').attr('src', imgsrc);
        // [HttpPost]
        $.post('?/ajaxCustomer/ajaxUpdateConfirmBabyHead/', {
            bid: btn.attr('data-id'),
            head: imgsrc
        });
    };

    /**
     * ajax获取用户生日列表
     * @param {type} limit
     * @returns {undefined}
     */
    customer.getBirthList = function(limit, callback) {
        limit = limit || 10;
        // [HttpPost]
        $.post('?/ajaxCustomer/ajaxGetCustomerBirthList/limit=' + limit, {}, function(r) {
            callback(r);
        });
    };

    /**
     * ajax搜索
     * @param {type} key
     * @returns {undefined}
     */
    customer.search = function(key, callback) {
        if (key !== '') {
            // [HttpPost]
            $.post('?/ajaxCustomer/search/key=' + key, {}, function(r) {
                callback(r);
            });
        }
    };

    /**
     * 获取客户id列表
     * @returns {undefined}
     */
    customer.getCuslistId = function(callback) {
        $.post('?/ajaxCustomer/getIdList/', {}, function(r) {
            callback(r);
        });
    };

    return customer;
});