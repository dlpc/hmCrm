
/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

define(['jquery'], function($) {
    var cl = {};
    
    /**
     * 提前加载card节点
     * @param {type} pageNo
     * @param {type} size
     * @param {type} callback
     * @returns {undefined}
     */
    cl.load = function(pageNo, size, callback) {
        $.get('?/Home/customer/page=' + pageNo + '&pagesize=' + size, function(r) {
            callback(r);
        });
    };
    
    return cl;
});