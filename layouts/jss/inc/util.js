
define(['jquery', 'spinner', 'fancyBox', 'jUploader'], function($, Spinner, fancyBox, jUploader) {

    var Util = {};

    fnFancyBox('#nav-consult-profile', function() {
        $.jUploader({
            button: $('#uhead'),
            action: '?/ajaxCustomer/ajaxUpdateHead/',
            onComplete: function(f, res) {
                var imgsrc = 'upload/' + res.imgn;
                $('#uhead img').attr('src', imgsrc);
                $('.tHead img').attr('src', imgsrc);
                // [HttpPost]
                $.post('?/ajaxConsult/updateHead/', {
                    head: imgsrc
                });
            }
        });
    });

    /**
     * datatable事件监听
     * @returns {undefined}
     */
    Util.dataTableLis = function(tableId, sel) {
        // 绑定节点集合
        var n;
        // 默认为.dTable
        tableId = tableId || '.dTable';
        // 是否可以选中 默认false
        DataTableSelect = sel || DataTableSelect;
        // 节点还是queryStr
        n = (typeof tableId === 'string') ? $(tableId + ' tbody tr') : $(tableId);
        n.unbind('click').click(function() {
            var node = $(this);
            if (DataTableSelect) {
                var cb = node.find('input:checkbox')[0];
                if (!DataTableMuli) {
                    // 单选
                    node.parent().find('tr.click').removeClass('click');
                    node.addClass('click');
                    node.parent().find('input:checked').each(function() {
                        this.checked = false;
                    });
                    if (cb) {
                        cb.checked = true;
                    }
                } else {
                    // 多选
                    if ($(this).hasClass('click')) {
                        $(this).toggleClass('click');
                        if (cb) {
                            cb.checked = false;
                        }
                    } else {
                        $(this).addClass('click');
                        if (cb) {
                            cb.checked = true;
                        }
                    }
                }
                cb = null;
            }
            $('.button.del,.button.edit').css('display', 'inline-block');
        }).mouseover(function() {
            $(this).addClass('hover');
        }).mouseout(function() {
            $(this).removeClass('hover');
        });
        if (DataTableSelect && DataTableMuli) {
            if (typeof tableId !== 'object') {
                $(tableId + ' thead tr .checkAll').unbind('click').click(function() {
                    var n = this;
                    $('tbody tr', $(this).parents('table')).find('input:checkbox').each(function(i, node) {
                        node.checked = n.checked;
                    });
                    if (n.checked) {
                        $('tbody tr', $(this).parents('table')).addClass('click');
                    } else {
                        $('tbody tr', $(this).parents('table')).removeClass('click');
                    }
                });
            }
        }
        tableId = null;
        n = null;
    };

    window.dataTableLis = Util.dataTableLis;

    Util.Alert = function(message, warn, callback) {
        warn = warn || false;
        var node = $('<div id="__alert__"></div>');
        if (warn) {
            node.addClass('warn');
        } else {
            node.removeClass('warn');
        }
        node.html(message);
        $('body').append(node);
        node.css('left', ($('body').width() - node[0].clientWidth) / 2 + 'px').slideDown();
        window.setTimeout(function() {
            node.slideUp(300, function() {
                if (typeof callback === 'function') {
                    callback();
                }
                $('#__alert__').remove();
            });
        }, 3000);
    };

    Util.dataTableLoading = function(query) {
        query = query || '.dTable';
        // <tr><td colspan="6" class="datatableLoading"> </td></tr>
        $('tbody', query).append('<tr class="rmd"><td colspan="6" class="datatableLoading"> </td></tr>');
    };

    Util.dataTableLoadingEnd = function(query, never) {
        query = query || '.dTable';
        never = never || false;
        $('.rmd', query).remove();
        if (!never)
            scrolling = false;
    };

    Util.scrollBottom = function(callback, offset) {
        if (typeof scrolling === 'undefined') {
            scrolling = false;
        }
        offset = offset || 150;
        $(window).scroll(function() {
            totalheight = parseFloat($(window).height()) + parseFloat($(window).scrollTop()) + offset;
            if ($(document).height() <= totalheight && !scrolling) {
                scrolling = true;
                callback();
            }
        });
    };

    Util.onresize = function(func) {
        if (typeof func === 'function') {
            $(window).on('resize', func);
            func();
        }
    };

    Util.JsonP = function(url, callback, error) {
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'jsonp',
            success: callback,
            error: error
        });
    };

    Util.viewAlbum = function() {
        var AjaxAlbum = $('.AjaxAlbum');
        AjaxAlbum.css('min-height', '100px');
        Spinner.spin($('.AjaxAlbum').get(0));
        var img = new Image(), heightx = 0;
        img.onload = function() {
            Spinner.stop();
            if (this.height > $(window).height() * 0.8) {
                heightx = $(window).height() * 0.8 + 'px';
            } else {
                heightx = this.height;
            }
            $('.AjaxAlbum').prepend('<img src="' + AjaxAlbum.attr('data-img') + '" height="' + heightx + '" />');
            $.fancybox.update();
            if ($('.AjaxAlbum p').html() !== '') {
                $('.AjaxAlbum p').fadeIn();
            }
        };
        img.src = AjaxAlbum.attr('data-img');
    };

    fnFancyBox('#nav-addconsult', function() {
        $('#addConsultBtn').click(function() {
            var name = $('#pd-consult-name').val();
            var sex = $('#pd-consult-sex').val();
            var email = $('#pd-consult-email').val();
            var phone = $('#pd-consult-phone').val();
            if (name !== '' && email !== '') {
                $.post('?/ajaxConsult/ajaxAdd/', {
                    name: name,
                    sex: sex,
                    email: email,
                    phone: phone
                }, function(r) {
                    if (r > 0) {
                        Util.Alert('添加成功,初始密码为 hm2015');
                    } else {
                        Util.Alert('添加失败，系统错误', true);
                    }
                    $.fancybox.close();
                });
            }
        });
    });

    return Util;
});

Object.onew = function(o) {
    var F = function(o) {
    };
    F.prototype = o;
    return new F;
};