<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class ImageUploader extends Model {

    const uploader = 'jUploaderFile';

    public $dir = '';

    /**
     * 文件上传处理
     * @return string|boolean
     */
    public function upload() {
        if (!empty($_FILES)) {
            $tempFile = $_FILES[self::uploader]['tmp_name'];
            $extension = $this->get_extension($_FILES[self::uploader]['name']);
            $targetFileName = uniqid(time()) . '.' . $extension;
            $targetFile = str_replace('//', '/', $this->dir) . $targetFileName;
            if (@move_uploaded_file($tempFile, $targetFile)) {
                chmod($targetFile, 0644);
                return $targetFileName;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * getExtension
     * @param type $file
     * @return type
     */
    function get_extension($file) {
        $info = pathinfo($file);
        return $info['extension'];
    }

}
