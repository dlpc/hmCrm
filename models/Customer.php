<?php

/**
 * 客户数据模型
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Customer extends Model {

    /**
     * 
     * @param type $page
     * @param type $pagesize
     * @param type $uid
     * @return boolean
     */
    public function getCustomerList($page = 0, $pagesize = 20, $uid = false) {
        if ($this->isDec($uid)) {
            return $this->Dao
                            ->select()
                            ->from(DBPREFIX . 'customers')
                            ->where("cus_uid=$uid")
                            ->orderby('id')->desc()
                            ->limit($page, $pagesize)
                            ->exec();
        } else {
            return $this->Dao
                            ->select()
                            ->from(DBPREFIX . 'customers')
                            ->orderby('id')->desc()
                            ->limit($page, $pagesize)
                            ->exec();
        }
        return false;
    }

    /**
     * 获取客户信息
     * @param type $cid
     * @return boolean
     */
    public function getCustomerInfo($cid) {
        if ($this->isDec($cid)) {
            return $this->Dao
                            ->select()
                            ->from(DBPREFIX . 'customers')
                            ->where("id=$cid")
                            ->getOneRow();
        }
        return false;
    }

    /**
     * 删除客户档案
     * @param type $cid
     * @return boolean
     */
    public function deleteCustomer($cid) {
        if ($this->isDec($cid)) {
            return $this->Dao
                            ->delete()
                            ->from(DBPREFIX . 'customers')
                            ->where("id=$cid")
                            ->exec();
        }
        return false;
    }

    /**
     * 添加客户数据
     * @param type $cid
     * @param type $dataFields
     * @return boolean
     */
    public function addCustomer($cid, $dataFields) {
        if ($this->isDec($cid)) {
            $this->toDec($cid);
            if ($cid == 0) {
                /**
                 * @param int $id == 0，为添加模式
                 */
                $ret = $this->Dao->insert(DBPREFIX . 'customers', 'cus_name, cus_age, cus_sex, cus_addate, cus_head, cus_birth')
                        ->values(array($dataFields['cus_name'], $dataFields['cus_age'], $dataFields['cus_sex'], 'NOW()', $dataFields['cus_head'], $dataFields['cus_birth']))
                        ->exec();
                $this->Dao->echoSql();
                return $ret;
            } else {
                /**
                 * @param int $id > 0，为编辑模式
                 */
                $id = abs(intval($id));
                return $this->Dao->update(DBPREFIX . 'journal')->set($dataFields)
                                ->where('cid=' . $cid)
                                ->exec();
            }
        } else {
            return false;
        }
    }

    /**
     * 获取客户生日列表
     * @param type $limit
     * @return array
     */
    public function getCustomerBirthList($limit = 20) {
        $ret = array();
        $today = date('m-d');
        $ulist = $this->Dao->select()->from(DBPREFIX . 'customers')
                ->where("DATE_FORMAT(cus_birth,'%m-%d') = '$today'")
                ->limit($limit)
                ->exec();
        $ret['count'] = count($ulist);
        $ret['list'] = $ulist;
        return $ret;
    }

    /**
     * 搜索客户列表
     * @param type $key
     * @return type
     */
    public function searchCustomer($key) {
        $ret = array();
        $ulist = $this->Dao->select()->from(DBPREFIX . 'customers')
                ->where("`cus_name` LIKE '%$key%'")
                ->exec();
        $ret['count'] = count($ulist);
        $ret['list'] = $ulist;
        return $ret;
    }

    /**
     * 
     * @param type $cid
     * @return type
     */
    public function getCustomerStat($cid) {
        $birth = $this->Dao->select('cus_birth')->from(DBPREFIX . 'customers')->where("id=$cid")->getOne();
        $y = date('Y');
        $m = date('m-d', strtotime($birth));
        $datetime1 = new DateTime($y . '-' . $m);
        $datetime2 = new DateTime(date('Y-m-d'));
        $interval = $datetime1->diff($datetime2);
        $baby = $this->Dao->select('')->count()->from(DBPREFIX . 'babys')->where("cid=$cid")->getOne();
        $journal = $this->Dao->select('')->count()->from(DBPREFIX . 'journal')->where("uid=$cid")->getOne();
        $album = $this->Dao->select('')->count()->from(DBPREFIX . 'album')->where("cid=$cid")->getOne();
        $remind = $this->Dao->select('')->count()->from(DBPREFIX . 'remind')->where("cid=$cid")->getOne();
        $idea = $this->Dao->select('')->count()->from(DBPREFIX . 'ideas')->where("cid=$cid")->getOne();
        return array(
            'baby' => $baby ? $baby : 0,
            'journal' => $journal ? $journal : 0,
            'album' => $album ? $album : 0,
            'remind' => $remind ? $remind : 0,
            'idea' => $idea ? $idea : 0,
            'birth' => $interval->format('%a')
        );
    }

}
