<?php

/**
 * 安全加密模块
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class Secure extends Model {

    /**
     * enCrypt
     * @global type $config
     * @param type $email
     * @param type $pwd
     * @return type
     */
    public function enCrypt($email, $pwd) {
        global $config;
        return hash('sha384', $pwd . $config->admin_salt . hash('md4', $email));
    }
    
    /**
     * encryptToken
     * @global type $config
     * @param type $ip
     * @param type $tstr
     */
    public function encryptToken($ip,$tstr){
        global $config;
        return hash('md4', $ip . $config->admin_salt . $tstr);
    }

}
