<?php

/**
 * 顾问数据模型
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Album extends Model {

    /**
     * 获取客户相册列表
     * @param type $cid
     * @return type
     */
    public function getAlbums($cid, $limit = false) {
        return $this->Dao->select()->from(DBPREFIX . 'album')->where("cid=$cid")->orderby('id')->desc()->limit($limit)->exec();
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function getAlbum($id) {
        return $this->Dao->select()->from(DBPREFIX . 'album')->where("id=$id")->getOneRow();
    }

    /**
     * 
     * @param type $cid
     * @param type $gname
     * @param type $gpath
     * @return boolean
     */
    public function addAlbum($cid, $gname, $gpath) {
        if (is_numeric($cid) && $cid > 0) {
            return $this->Dao->insert(DBPREFIX . 'album', 'cid,gname,gpath,date')->values(array(
                        $cid, $gname, $gpath, 'NOW()'
                    ))->exec();
        }
        return false;
    }

    /**
     * 
     * @param type $cid
     * @param type $albumId
     * @return boolean
     */
    public function deleteAlbum($cid, $albumId) {
        if (is_numeric($cid) && $cid > 0 && $albumId > 0) {
            return $this->Dao->delete()->from(DBPREFIX . 'album')->where("cid = $cid AND `id` = $albumId")->exec();
        }
        return false;
    }

}
