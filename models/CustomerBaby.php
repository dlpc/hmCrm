<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class CustomerBaby extends Model {

    /**
     * 删除宝宝
     * @param type $bid
     * @return type
     */
    public function deleteBaby($bid) {
        return $this->Dao->delete()->from(DBPREFIX . 'babys')->where('id=' . $bid)->exec();
    }

    /**
     * 获取某宝宝信息
     * @param type $bid
     * @return type
     */
    public function getBaby($bid) {
        return $this->Dao->select()->from(DBPREFIX . 'babys')->where('id=' . $bid)->getOneRow();
    }
    
    /**
     * 获取客户宝宝列表
     * @param type $cid
     * @return type
     */
    public function getCustomerBabys($cid) {
        return $this->Dao->select()->from(DBPREFIX . 'babys')->where('cid=' . $cid)->exec();
    }

    /**
     * 编辑宝宝信息
     * @param type $bid
     * @param type $dataFields
     * @return boolean
     */
    public function alterCustomerBaby($bid, $dataFields) {
        if (is_numeric($bid)) {
            $this->toDec($bid);
            if ($bid == 0) {
                /**
                 * @param int $id == 0，为添加模式
                 */
                $ret = $this->Dao->insert(DBPREFIX . 'babys', 'b_name, b_birth, b_sex, cid')
                        ->values(array($dataFields['b_name'], $dataFields['b_age'], $dataFields['b_sex'], $dataFields['cid']))
                        ->exec();
                $this->Dao->echoSql();
                return $ret;
            } else {
                /**
                 * @param int $id > 0，为编辑模式
                 */
                $bid = abs(intval($bid));
                return $this->Dao->update(DBPREFIX . 'babys')->set($dataFields)
                                ->where('id=' . $bid)
                                ->exec();
            }
        } else {
            return false;
        }
    }

}
