<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Remind extends Model {

    /**
     * 获取remind
     * @param type $cid
     * @return type
     */
    public function gets($cid) {
        $ret = $this->Dao->select()->from(DBPREFIX . 'remind')->where("cid=$cid")->orderby('id')->desc()->exec();
        foreach ($ret as &$r) {
            $r['dateX'] = $this->Util->dateTimeFormat($r['date']);
        }
        return $ret;
    }

}
