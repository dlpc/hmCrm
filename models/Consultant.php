<?php

/**
 * 顾问数据模型
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class Consultant extends Model {

    /**
     * 
     * @param type $uid
     */
    public function isConsultantExist($uid) {
        if (is_numeric($uid) && intval($uid) > 0) {
            $ret = $this->Dao
                    ->select()
                    ->count()
                    ->from(DBPREFIX . 'onsultant')
                    ->where('uid=' . $uid)
                    ->getOne();
            return $ret > 0;
        } else {
            return false;
        }
    }

    /**
     * getUser data
     * @param type $uid
     * @return type
     */
    public function getConsultant($uid) {
        return $this->Dao
                ->select()
                ->from(DBPREFIX . 'onsultant')
                ->where('uid=' . $uid)
                ->exec();
    }

    /**
     * 添加一个新顾问
     * @param type $uid
     * @param type $datafield
     * @return boolean
     */
    public function addConsultant($uid, $datafield) {
        if ($uid < 0) {
            // delete user
            return $this->deleteUser($uid);
        } else {
            return $this->Dao
                    ->insert('onsultant', '(`user_name`,`user_sex`)')
                    ->values($datafield)
                    ->exec();
        }
    }

    /**
     * 删除一个顾问数据
     * @param type $uid
     * @return type
     */
    public function deleteConsultant($uid) {
        return $this->Dao
                ->delete()
                ->from(DBPREFIX . 'onsultant')
                ->where('uid=' . $uid)
                ->exec();
    }

    /**
     * 获取顾问Id
     * @param type $email
     * @return type
     */
    public function getConsultantIdByEmail($email) {
        return $this
                ->select('id')
                ->from(DBPREFIX . 'onsultant')
                ->where('`email=' . $email . '`')
                ->getOne();
    }

    /**
     * 校验ConsultanToken
     * @param type $token
     * @param type $email
     * @return boolean
     */
    public function verify($token, $email) {
        $ip = $this->getIp();
        $pToken = $this->Secure->encryptToken($ip, $email);
        return $pToken === $token;
    }

}
