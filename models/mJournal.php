<?php

/**
 * 客户日志模块
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class mJournal extends Model {

    /**
     * 获取最新日志列表
     * @param type $limit
     */
    public function getAllJournals($limit = 5, $uid = 1) {
        $ret = $this->Dao->select()->from(DBPREFIX . 'journal')->alias('j')
                        ->leftJoin(DBPREFIX . 'customers')->alias('cus')->on('`cus`.id = `j`.uid and `cus`.id > 0')
                        ->having('`cus`.cus_uid = ' . $uid)
                        ->orderby('`j`.id')->desc()
                        ->limit($limit)->exec();
        return $ret;
    }

    /**
     * 获取用户所有日志列表
     * @param type $uid
     * @return boolean
     */
    public function getUserJournalList($uid) {
        if ($this->isDec($uid)) {
            $ret = $this->Dao->select()->from(DBPREFIX . 'journal')->where('uid=' . $uid)->orderby('id')->desc()->exec();
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * 获取用户最新一篇日志
     * @param type $uid
     * @return boolean
     */
    public function getUserLastJournal($uid) {
        if ($this->isDec($uid)) {
            $ret = $this->Dao->select()->from(DBPREFIX . 'journal')->where('uid=' . $uid)->orderby('id')->desc()->limit(1)->getOneRow();
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * 获取客户日志
     * @param type $cusid
     * @return boolean
     */
    public function getCustomerJournalList($cusid, $pageNo = 0, $pageSize = 15) {
        if ($this->isDec($cusid)) {
            $ret = $this->Dao->select()->from('journal')->where('cusid=' . $cusid)->orderby('id')->desc()->limit($pageNo, $pageSize)->exec();
            $this->Dao->echoSql();
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * 删除销售日志
     * @param type $id
     * @return boolean
     */
    public function deleteJournal($id) {
        if ($this->isDec($id)) {
            $ret = $this->Dao->delete()->from(DBPREFIX . 'journal')->where("id=$id")->exec();
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * 添加客户日志
     * @param type $uid
     * @param type $cid
     * @param type $title
     * @param type $content
     * @return boolean
     */
    public function addJournal($id, $uid, $cid, $title, $content) {

        if ($this->isDec($uid) && $this->isDec($cid) && !empty($content)) {
            if (intval($id) == 0) {
                /**
                 * @param int $id == 0，为添加模式
                 */
                return $this->Dao->insert(DBPREFIX . 'journal', 'uid, jtitle, jdate, jcont, cusid')->values(array($cid, $title, 'NOW()', $content, $uid))->exec();
            } else {
                /**
                 * @param int $id > 0，为编辑模式
                 */
                $id = abs(intval($id));
                $ret = $this->Dao->update(DBPREFIX . 'journal')->set(array(
                            'uid' => $uid,
                            'jtitle' => $title,
                            'jcont' => $content,
                            'cusid' => $cid
                        ))->where('id=' . $id)->exec();
                $this->Dao->echoSql();
                return $ret;
            }
        } else {
            return false;
        }
    }

}
