<?php

/**
 * Desc
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

/**
 * Description of Feeds
 *
 * @author Administrator
 */
class Feeds extends Model {

    const FEED_TYPE_ALBUM = 0;
    const FEED_TYPE_JOURNAL = 1;
    const FEED_TYPE_BABY = 2;

    /**
     * Feeds Add
     * @param type $cid
     * @param type $ftype
     * @param type $fhash
     * @param type $uid
     * @return boolean
     */
    public function add($cid, $uid, $ftype, $fhash, $fid) {
        if ($cid > 0 && $uid >= 0) {
            if (!is_array($fhash)) {
                $ret = $this->Dao->insert(DBPREFIX . 'newfeeds', 'ftype, fhash, ftime, fid, uid, cid')->values(array($ftype, $fhash, 'NOW()', $fid, $uid, $cid))->exec();
            } else {
                $ret = $this->Dao->insert(DBPREFIX . 'newfeeds', 'ftype, fhash, fhash1, ftime, fid, uid, cid')->values(array($ftype, $fhash[0], $fhash[1], 'NOW()', $fid, $uid, $cid))->exec();
            }
            #$this->Dao->echoSql();
            return $ret;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param type $cid
     * @return type
     */
    public function get($cid) {
        $ret = $this->Dao->select()->from(DBPREFIX . 'newfeeds')->alias('nf')
                ->leftJoin(DBPREFIX . 'consultant')->alias('cn')
                ->on('cn.id = nf.uid')
                ->where("cid=$cid")
                ->orderby('nf.id')->desc()
                ->exec();
        foreach ($ret as &$r) {
            $r['Ftime'] = $this->Util->dateTimeFormat($r['ftime']);
        }
        return $ret;
    }

}
