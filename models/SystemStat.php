<?php

/**
 * 客户数据模型
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class SystemStat extends Model {

    public function getSystemStat() {
        if ($this->meInfo['lev'] > 0) {
            $uid = $this->getHid();
            $in = array();
            $customers = $this->Dao->select()->from(DBPREFIX . 'customers')->where("cus_uid=$uid")->exec();
            $customer = $this->Dao->select('')->count()->from(DBPREFIX . 'customers')->where("cus_uid=$uid")->getOne();
            foreach ($customers as $c) {
                $in[] = $c['id'];
            }
            $in = '(' . implode(',', $in) . ')';
            $baby = $this->Dao->select('')->count()->from(DBPREFIX . 'babys')->where("cid In $in")->getOne();
            $album = $this->Dao->select('')->count()->from(DBPREFIX . 'album')->where("cid In $in")->getOne();
            $journal = $this->Dao->select('')->count()->from(DBPREFIX . 'journal')->where("uid In $in")->getOne();
        } else {
            $customer = $this->Dao->select('')->count()->from(DBPREFIX . 'customers')->getOne();
            $baby = $this->Dao->select('')->count()->from(DBPREFIX . 'babys')->getOne();
            $album = $this->Dao->select('')->count()->from(DBPREFIX . 'album')->getOne();
            $journal = $this->Dao->select('')->count()->from(DBPREFIX . 'journal')->getOne();
        }
        return array(
            'customer' => $customer > 0 ? $customer : 0,
            'baby' => $baby > 0 ? $baby : 0,
            'album' => $album > 0 ? $album : 0,
            'journal' => $journal > 0 ? $journal : 0
        );
    }

}
