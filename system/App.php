<?php

/**
 * 系统路由
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */

class App {

    // Singleton instance
    protected static $_instance = NULL;
    
    // Controller instance
    public $Controller = NULL;

    /**
     * get App Class instance
     * @return $_instance
     */
    public static function getInstance() {
        if (!self::$_instance instanceof self) {
            self::$_instance = new App();
        }
        return self::$_instance;
    }

    /**
     * 
     * @param type $QueryString
     * @return Object QueryObject
     */
    private function packQueryString($QueryString) {
        if (!empty($QueryString)) {
            $QueryObject = new stdClass();
            $QueryString = explode('&', $QueryString);
            foreach ($QueryString as $r) {
                $r = explode('=', $r);
                if (count($r) == 2) {
                    $QueryObject->$r[0] = $r[1];
                }
            }
            return $QueryObject;
        } else {
            return NULL;
        }
    }

    /**
     * action转换
     * @param type $config
     * @param type $Controller
     * @param type $Action
     * @return type
     */
    private function getAction($config, $Controller, $Action) {
        if ($Action == "") {
            if (array_key_exists($Controller, $config->defaultAction)) {
                return $config->defaultAction[$Controller];
            } else {
                return 'index';
            }
        } else {
            // Action&querystring
            if (strstr($Action, "&")) {
                return substr($Action, 0, strpos($Action, "&"));
            }
            return $Action;
        }
    }

    /**
     * @global type $config
     * parse http request
     */
    public function parseRequest() {
        global $config;
        // Route the Controller and queryString
        $URI = $this->uriDecom($config);
        $RouteParam = new stdClass();
        $RouteParam->queryString = "";
        if (isset($GLOBALS['controller'])) {
            $RouteParam->controller = $GLOBALS['controller'];
            $Action = $this->getAction($config, $RouteParam->controller, $GLOBALS['action']);
        } else {
            if ($URI[1] == "" || strpos($URI[1], '=')) {
                $RouteParam->controller = $config->default_controller;
                if (strpos($URI[1], '=')) {
                    $RouteParam->queryString = $URI[1];
                }
            } else {
                $RouteParam->controller = $URI[1];
                $RouteParam->queryString = isset($URI[3]) ? $URI[3] : '';
            }
            $Action = $this->getAction($config, $RouteParam->controller, isset($URI[2]) && preg_match("/\w+\_?\w?/is", $URI[2]) ? $URI[2] : "");
        }
        // 路由动作
        $RouteParam->action = $Action;
        
        $this->Controller = new $RouteParam->controller($RouteParam->controller, $RouteParam->action, $RouteParam->queryString);

        $this->Controller->modulePreload();
        
        $this->Controller->init();

        // 注册当前URI
        $this->Controller->uri = $URI = preg_replace('/\/\?\/$/', '', "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        // 回调根目录
        $this->Controller->root = "http://" . $_SERVER['HTTP_HOST'] . $config->shoproot;
        // 调用对应方法
        $this->Controller->$Action($this->packQueryString($RouteParam->queryString));
    }

    /**
     * url分隔，缓存360秒
     * @access private
     * @param type $config
     * @return type
     */
    private function uriDecom($config) {
        $URI = $this->_uriDecom($config);
        return $URI;
    }

    /**
     * url分割子函数
     * @access private
     * @return Array
     */
    private final function _uriDecom($config) {
        $URI = str_replace('index.php', '', $_SERVER["REQUEST_URI"]);
        $URI = str_replace('/?/', '/', $URI);
        $URI = preg_replace('/\/\?\/$/', '', $URI);
        $URI = str_replace($config->shoproot, '/', $URI);
        $URI = str_replace('index.php', '', $URI);
        $URI = str_replace('?', '', $URI);
        $URI = explode('/', $URI);
        return $URI;
    }

}
