<?php

/**
 * MVC模型超类
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Model {

    private $Controller;

    // 构造方法
    public function __construct() {
        
    }

    function __get($name) {
        $class = $this->Controller;
        if (property_exists($class, $name)) {
            return $this->Controller->$name;
        }
    }

    /**
     * 判断是否正整数
     * @param type $fackInt
     * @return type
     */
    function isDec($fackInt) {
        return is_numeric($fackInt) && intval($fackInt) >= 0;
    }

    /**
     * 整数指针转换
     * @param type $fackInt
     */
    function toDec(&$fackInt) {
        $fackInt = intval($fackInt);
    }

    /**
     * magic call
     * @param type $name
     * @param type $arguments
     */
    function __call($name, $arguments) {
        // 对Controller进行动态反射，跨对象调用
        $class = new ReflectionClass('Controller');
        try {
            $ec = $class->getMethod($name);
            return $ec->invokeArgs($this->Controller, $arguments);
        } catch (ReflectionException $re) {
            die('Fatal Error : ' . $re->getMessage());
        }
        return false;
    }

    /**
     * hook
     * 手动挂载model到另外一个model
     * @todo 自动挂载
     */
    public function hook($classA = array()) {
        foreach ($classA AS $class) {
            $className = get_class($class);
            $this->$className = $class;
            unset($className);
        }
    }

    public function linkController($obj) {
        $this->Controller = $obj;
    }

}
