<?php

/**
 * MVC控制器超类
 * @description Holp You Do Good But Not Evil
 * @copyright   Copyright 2014-2015 <ycchen@iwshop.cn>
 * @license     LGPL (http://www.gnu.org/licenses/lgpl.html)
 * @author      Chenyong Cai <ycchen@iwshop.cn>
 * @package     Wshop
 * @link        http://www.iwshop.cn
 */
class Controller {

    // 模板引擎句柄
    public $Smarty;
    // ActionName
    private $Action;
    // ControllerName
    private $ControllerName;
    // QueryString
    private $QueryString;
    // currentURI
    public $uri;
    // Smarty Cache Id
    public $cacheId = null;
    // Smarty TplName
    public $TplName;
    // Now
    public $now;
    // Settings
    public $settings;

    const VPAR_RES_GET = 0;
    const VPAR_RES_POST = 1;
    const VPAR_RES_COOKIE = 2;

    /**
     * 
     * @global type $config
     * @param type $ControllerName
     * @param type $Action
     * @param type $QueryString
     */
    public function __construct($ControllerName, $Action, $QueryString) {
        global $config;
        // Params
        $this->ControllerName = $ControllerName;
        $this->Action = $Action;
        $this->QueryString = $QueryString;
        $this->now = time();
        // Smarty
        $this->Smarty = new Smarty();
        // Smarty caching
        if ($config->Smarty['cached']) {
            $this->Smarty->caching = true;
            $this->Smarty->cache_lifetime = $config->Smarty['cache_lifetime'];
            $this->Smarty->setCacheDir(dirname(__FILE__) . '/../tmp/tpl_cache/');
        }
        // Smarty TemplateDir
        $this->Smarty->setTemplateDir(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $config->tpl_dir . DIRECTORY_SEPARATOR);
        // Smarty CompileDir
        $this->Smarty->setCompileDir(dirname(__FILE__) . '/../tmp/tpl_compile/');
        // css version
        $this->Smarty->assign('cssversion', date('Y-m-d-H-i'));
        // root
        $this->Smarty->assign('docroot', $config->shoproot);
        // root
        $this->Smarty->assign('config', (array) ($config));
        // searchkey
        $this->Smarty->assign('searchkey', '');
        // inwechat
        $this->Smarty->assign('inWechat', $this->inWechat());
        // pageStr
        $this->Smarty->assign('controller', $this->ControllerName);
        // Tplname
        $this->TplName = '.' . DIRECTORY_SEPARATOR . strtolower($this->ControllerName) . DIRECTORY_SEPARATOR . strtolower($this->Action) . '.tpl';
    }

    /**
     * 模块预加载
     * @global type $config
     */
    public function modulePreload() {
        global $config;
        if ($config->preload) {
            foreach ($config->preload as $_preload) {
                $this->loadModel($_preload);
            }
        }
    }

    /**
     * 模板渲染
     */
    public function show($tpl_name = false) {
        /**
         * 模板文件名判断，必须区分控制器目录。
         * 如果指定目录，则查找view目录
         */
        $tpl_name = !$tpl_name ? $this->Action : $tpl_name;
        // 带目录路径
        if ($tpl_name && preg_match('/\//', $tpl_name)) {
            $this->Smarty->display($tpl_name, $this->cacheId);
        } else {
            $this->Smarty->display(strtolower($this->ControllerName . '_' . $tpl_name) . '.tpl', $this->cacheId);
        }
    }

    /**
     * 
     * @param type $cacheId
     * @param type $tplName
     * @return type
     */
    public final function isCached($cacheId = null, $tplName = null) {
        return $this->Smarty->isCached($tplName ? $tplName : $this->TplName, $cacheId ? $cacheId : $this->cacheId);
    }

    /**
     * 加载模型
     * @param type $modelName
     * @return stdClass
     */
    public function loadModel($modelName) {
        if (!isset($this->$modelName)) {
            if (property_exists($modelName, 'instance')) {
                // 单例模式
                $this->$modelName = $modelName::get_instance();
            } else {
                $this->$modelName = new $modelName();
            }
            $this->$modelName->linkController($this);
        } else {
            
        }
        return $this->$modelName;
    }

    /**
     * 判断是否在微信浏览器
     * @return type
     */
    final public static function inWechat() {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false;
    }

    /**
     * include path
     */
    protected function add_include_path($path) {
        set_include_path($path . get_include_path());
    }

    /**
     * getIPaddress
     * @return type
     */
    final public function getIp() {
        return $this->Util->getIp();
    }

    /**
     * print json from array
     * @param type $arr
     */
    final public function echoJson($arr) {
        @header('Content-Type:application/json');
        print_r(json_encode($arr));
    }

    /**
     * to Json Str
     * @param type $arr
     * @return type
     */
    final public function toJson($arr) {
        return print_r(json_encode($arr), true);
    }

    /**
     * 获取GET参数
     * @param type $name
     * @param type $default
     * @return type
     */
    public function pGet($name = false, $default = false) {
        return $this->getpostV($name, $default, $_GET);
    }

    /**
     * 获取POST参数
     * @param type $name
     * @param type $default
     * @return type
     */
    public function pPost($name = false, $default = false) {
        return $this->getpostV($name, $default, $_POST);
    }

    /**
     * 
     * @param type $name
     * @param type $default
     * @return String
     */
    public function pCookie($name = false, $default = false) {
        if (!isset($_COOKIE[$name])) {
            return false;
        } else {
            return $this->getpostV($name, $default, $_COOKIE);
        }
    }

    /**
     * 
     * @param string $key
     * @param mixed $value
     * @param int $exp
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function sCookie($key, $value, $exp = 36000, $path = NULL, $domain = NULL) {
        return setcookie($key, $value, $this->now + $exp, $path, $domain);
    }

    /**
     * 
     * HttpOnly使得js无法读取cookie内容，防止xss
     * @param string $key
     * @param mixed $value
     * @param int $exp
     * @return bool <b>TRUE</b> on success or <b>FALSE</b> on failure.
     */
    public function sCookieHttpOnly($key, $value, $exp = 36000, $path = NULL, $domain = NULL) {
        return setcookie($key, $value, $this->now + $exp, $path, $domain, false, true);
    }

    /**
     * GET || POST Filter
     * @param type $name
     * @param type $default
     * @param type $retSet
     * @param type $isGet
     * @return boolean
     */
    private function getpostV($name, $default = false, $retSet = array()) {
        // empty or null
        if (!$name || empty($retSet)) {
            // return false
            return false;
        } else if ((!$default && !isset($retSet[$name]))) {
            // if default value isseted then
            // return default value
            return $default;
        } else {
            // return the filted value
            return $this->Util->xssFilter($retSet[$this->Util->xssFilter($name)]);
        }
    }

    /**
     * report error
     * @param type $msg
     * @param type $filename
     */
    final public function reportError($msg, $filename) {
        include_once(dirname(__FILE__) . "/../models/WechatSdk.php");
        $stoken = WechatSdk::getServiceAccessToken();
        Messager::sendText($stoken, DEVREPORT_OPENID, date("Y-m-d h:i:sa") . ' 错误信息' . $msg . ' 文件路径：' . $filename);
    }

    /**
     * 获取server参数
     * @param type $name
     * @return type
     */
    final public function server($name = false) {
        if ($name !== false) {
            return $_SERVER[$name];
        } else {
            return $_SERVER;
        }
    }

    /**
     * 获取post参数
     * @param type $name
     * @return type
     */
    final public function post($name) {
        return $_POST[$name];
    }

    /**
     * decode unicode
     * @param type $str
     * @return type
     */
    function unIescape($str) {
        return str_replace('\/', '/', preg_replace("#\\\u([0-9a-f]+)#ie", "iconv('UCS-2', 'UTF-8', pack('H4', '\\1'))", $str));
    }

    /**
     * 跳转
     * @param type $href
     */
    function redirect($href) {
        header("Location:$href");
        exit(0);
    }

    /**
     * SQL注入过滤
     * @param type $Str
     * @return type
     */
    public function sqlEscape($Str) {
        return addslashes(mysql_real_escape_string($Str));
    }

    /**
     * 判断是否正整数
     * @param type $fackInt
     * @return type
     */
    function isDec($fackInt) {
        return is_numeric($fackInt) && intval($fackInt) > 0;
    }

    /**
     * 整数指针转换
     * @param type $fackInt
     */
    function toDec(&$fackInt) {
        $fackInt = intval($fackInt);
    }

    /**
     * 解密uid密文hid并返回
     * @return type
     */
    function getHid() {
        $uid = $this->pCookie('hid');
        if ($uid !== false) {
            $uid = $this->Util->digDecrypt($uid);
        }
        return $uid;
    }

    function tLogin() {
        if (!isset($_COOKIE['uid'])) {
            $this->redirect($this->config->docroot . '?/Login/ajaxLogout/');
        }
    }

    public function init() {
        $id = $this->getHid();
        if ($id) {
            $this->meInfo = $this->Dao->select()->from(DBPREFIX . 'consultant')->where("id=$id")->getOneRow();
            $this->Smarty->assign('meInfo', $this->meInfo);
        }
    }

}
