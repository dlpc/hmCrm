<div id="topbar">
    <div class="in">
        <a href="{$docroot}" class="logo"><img src="layouts/images/logo.png" width="80px" /></a>
        <div class="search-w-box">
            <input type="search" 
                   id="searchBox"
                   class="search-w-input"
                   value=""
                   placeholder="搜一搜，找到你想要的" />
            <div id="searchResult">
                <div class="ret">
                    <a class="close" href="javascript:;" onclick="$('#searchResult').fadeOut();"></a>
                    <i class="arrow"></i>
                    <i class="arrow2"></i>
                    <div></div>
                </div>
            </div>
        </div>
        <div id="tnav">
            <a class="tHead fancybox.ajax hidden" data-fancybox-type="ajax" href="?/ajaxConsult/vProfile/" id="nav-consult-profile">
                <img src="{$meInfo.chead}" />
            </a>
            {if $meInfo.lev eq 0}<a class="navbtn fancybox.ajax hidden" data-fancybox-type="ajax" href="?/ajaxConsult/add/" id="nav-addconsult">+新合伙人</a>{/if}
            <a class="navbtn fancybox.ajax hidden" data-fancybox-type="ajax" href="?/ajaxCustomer/editProfile/" id="nav-addcustomer">+新客户</a>
        </div>
    </div>
</div>