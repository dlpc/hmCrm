<!DOCTYPE html>
<html>
    <head>
        <title>HMCRM</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/home.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="Stylesheet" />
        <script src="layouts/jss/jquery-2.1.1.min.js"></script>
        <script data-main="layouts/jss/inc/profile-list.js?v={$cssversion}" src="layouts/jss/require.min.js"></script>
    </head>
    <body class="profileView">
        <input type="hidden" id="cusid" value="{$cus.id}" />
        {include file="./inc/topnav.tpl"}
        <div class="profileList clearfix">
            <div class="left">
                <div class="wList">
                    <div class="list">
                        <div class="Thead"><span>Customers</span></div>
                        {foreach from=$list item=cus}
                            <div class="cuslist clearfix">
                                <a class="head" href="?/Profile/view/id={$cus.id}"><img src="{$cus.cus_head}" /></a>
                                <div class="info">
                                    <a href="?/Profile/view/id={$cus.id}"><b>{$cus.cus_name}</b></a>
                                    <p>{$cus.lastJournal.jcont}</p>
                                    <p class="tips">
                                        <a href="?/Profile/view/id={$cus.id}">Babys(<b>{$cus.stat.baby}</b>)</a>
                                        <a href="?/Profile/view/id={$cus.id}">Photos(<b>{$cus.stat.album}</b>)</a>
                                        <a href="?/Profile/view/id={$cus.id}">Journals(<b>{$cus.stat.journal}</b>)</a>
                                        <a href="?/Profile/view/id={$cus.id}">Reminds(<b>{$cus.stat.remind}</b>)</a>
                                        <a href="?/Profile/view/id={$cus.id}">Ideas(<b>{$cus.stat.idea}</b>)</a>
                                    </p>
                                    {if $cus.uname neq ''}
                                        <div class="cname">{$cus.uname}</div>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="wList">
                    <div class="list">
                        <div class="Thead"><span>Statistics</span></div>
                        <div id="sysstat" class="clearfix">
                            <div class="pases"><b>{$stat.customer}</b>Customers</div>
                            <div class="pases"><b>{$stat.baby}</b>Babys</div>
                            <div class="pases"><b>{$stat.album}</b>Photos</div>
                            <div class="pases"><b>{$stat.journal}</b>Journals</div>
                        </div>
                    </div>
                </div>
                <div class="wList">
                    <div class="list">
                        <div class="Thead"><span>New Journals</span></div>
                        {foreach from=$newJournal item=journal}
                            <div class="lsCusJournal clearfix">
                                <a class="head" href="?/Profile/view/id={$journal.uid}"><img src="{$journal.cus_head}" /></a>
                                <div class="cont">
                                    <a class="name" href="?/Profile/view/id={$journal.uid}">{$journal.cus_name}</a>
                                    <p><span class="date">{$journal.jdate}</span> {$journal.jcont}</p>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('img').on('error', function() {
                $(this).attr('src', 'layouts/images/icons/iconfont-error.png');
            });
        </script>
    </body>
</html>