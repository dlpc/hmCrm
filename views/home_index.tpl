<!DOCTYPE html>
<html>
    <head>
        <title>海米CRM</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/home.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="Stylesheet" />
        <script data-main="layouts/jss/inc/homeindex.js?v={$smarty.now}" src="layouts/jss/require.min.js"></script>
    </head>
    <body style="overflow: hidden">
        <iframe id="inframe" src="" style="display: block;margin:0 auto;" width="1050px" frameborder="0"></iframe>
        <div id="inframeLoading"></div>
    </body>
</html>