<form id="editBabyProfile">
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">姓名</div>
            <div class="gs-text">
                <input type="text" name="b_name" value="{if $ed}{$b.b_name}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">性别</div>
            <select name="b_sex">
                <option value="男" {if $ed and $b.b_sex eq '男'}selected{/if}>男</option>
                <option value="女" {if $ed and $b.b_sex eq '女'}selected{/if}>女</option>
            </select>
        </div>
    </div>
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">年龄</div>
            <div class="gs-text">
                <input type="text" name="b_age" value="{if $ed}{$b.b_age}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">生日</div>
            <div class="gs-text">
                <input type="text" name="b_birth" value="{if $ed}{$b.b_birth}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
    </div>
    <div class="gs-label">备注</div>
    <textarea class="mpdcont" name='b_remark'>{if $ed}{$b.b_remark}{/if}</textarea>
    <div class="textAlignCenter">
        <a id="addJournalBtn" onclick="editBabyProfileClick({if $ed}{$b.id}{else}0{/if})" href="javascript:;" class="wd-btn primary">Done</a>
    </div>
</form>