<!DOCTYPE html>
<html>
    <head>
        <title>HMCRM</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/albumlist.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <script type="text/javascript" src="layouts/jss/jquery-2.1.1.min.js"></script>
        <script data-main="layouts/jss/inc/valbum.js?v={$cssversion}" src="layouts/jss/require.min.js"></script>
    </head>
    <body class="albumView">
        <input type="hidden" id="cusid" value="{$cus.id}" />
        {include file="./inc/topnav.tpl"}
        <div id="TimeLine">
            {foreach from=$albums item=album key=k}
                <div class="wrap clearfix">
                    <div class="time">{$k}</div>
                    {*                    {$k}*}
                    <div class="item clearfix">
                        <span class="point"></span>
                        <i class="arrow2"></i>
                        {foreach from=$album item=albumx}
                            <a class='photo fancybox.ajax' data-fancybox-type="ajax" href="?/vAlbum/v/id={$albumx.id}">
                                <img src='layouts/Thumbnail/?p=/upload/{$albumx.gpath}&w=400' alt="{$albumx.gname}" title="{$albumx.gname}" />
                            </a>
                        {/foreach}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</body>
</html>
