<!DOCTYPE html>
<html>
    <head>
        <title>HMCRM</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/home.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="Stylesheet" />
        <script type="text/javascript" src="layouts/jss/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="layouts/jss/masonry.pkgd.min.js"></script>
        <script data-main="layouts/jss/inc/profile.js?v={$cssversion}" src="layouts/jss/require.min.js"></script>
    </head>
    <body class="profileView">
        <input type="hidden" id="cusid" value="{$cus.id}" />
        {include file="./inc/topnav.tpl"}
        <div class="cusinfo clearfix">
            <div class="cusleft">

                <!-- Customer Profle -->
                <div class="cusProfile clearfix">
                    <a class="jUpload" href="javascript:;" data-id="{$cus.id}"><img class="cushead" src="{if $cus.cus_head neq ''}{$cus.cus_head}{else}layouts/Thumbnail/?p=/layouts/images/profle_1.png&h=250&w=250{/if}" /></a>
                    <div class="profile">
                        <div class='pl clearfix'>{include file='./ajaxcustomer_getcustomerinfo.tpl'}</div>
                        <div class="edit hidden">
                            <a class="addProfile fancybox.ajax hidden" data-fancybox-type="ajax" href="?/ajaxCustomer/editProfile/">Edit</a>&nbsp;
                            <a class="editProfile fancybox.ajax" data-fancybox-type="ajax" href="?/ajaxCustomer/editProfile/id={$cus.id}">Edit</a>&nbsp;
                            <a class='delete' href='javascript:;' onclick='fnDeleteCustomer({$cus.id})'>Delete</a>
                        </div>
                    </div>
                </div>

                <div class="clearfix">

                    <div id="remindme" class="wList" style="float:left;width:49%;">
                        <div class="list">
                            <div class="Thead"><span>Remind Me</span></div>
                            <div id='remindList'>
                                {include file='./profile_remind.tpl'}
                            </div>
                        </div>
                        <a id="wRemind" class="fixAddBtn fancybox.ajax" data-fancybox-type="ajax" href="layouts/static/addRemind.html"> </a>
                    </div>

                    <div id="createideas" class="wList" style="float:right;width:49%;">
                        <div class="list">
                            <div class="Thead"><span>Creative Ideas</span></div>
                            <div id='ideaList'>
                                {include file='./profile_ideas.tpl'}
                            </div>
                        </div>
                        <a id="wIdeas" class="fixAddBtn fancybox.ajax" data-fancybox-type="ajax" href="layouts/static/addIdeas.html"> </a>
                    </div>
                </div>

                <div class="journal wList">
                    <div class="Thead"><span>Journals</span></div>
                    <div id='journalList'>
                        {include file="./ajaxjournal_ajaxgetjournallist.tpl"}
                    </div>
                    <a class="addjournal fixAddBtn fancybox.ajax" data-fancybox-type="ajax" href="layouts/static/addJournal.html"> </a>
                </div>

                {*                <!-- Customer Album -->
                <div class='cusAlbum' id="ALBUM">
                <div class="gutter-sizer"></div>
                <a id="uploadComplete" href="#albumAddFrame"></a>
                <a id='albumsAdd' class="albums" href="javascript:;" data-id="{$cus.id}"></a>
                {include file='./profile_albums.tpl'}
                </div>

                *}

                {*                <div class="journal wList" id="journal-{$cus.id}">
                <div class="Thead" style="padding-left:0;"><span>News Feed</span></div>
                <div class="feeds">
                {foreach from=$cus.feeds item=feed}
                {include file="./inc/feed_ftype{$feed.ftype}.tpl"}
                {/foreach}
                </div>
                <a class="addjournal fixAddBtn fancybox.ajax" data-fancybox-type="ajax" href="layouts/static/addJournal.html"> </a>
                </div>*}

            </div>
            <div class="cusright">

                <div class="wList">
                    <div class="list">
                        <div class="Thead"><span>Statistics</span></div>
                        <div id="sysstat" class="clearfix">
                            <div class="pases"><b style="color:#44b549">{$cus.stat.birth}<i>Days</i></b>BirthDay</div>
                            <div class="pases"><b>{$cus.stat.baby}</b>Babys</div>
                            <div class="pases"><b>{$cus.stat.album}</b>Photos</div>
                            <div class="pases"><b>{$cus.stat.journal}</b>Journals</div>
                        </div>
                    </div>
                </div>

                <div class="cusbabys">
                    {include file='./ajaxbaby_getbabylist.tpl'}
                </div>

                <div class="wList">      
                    <a id="albumsAdd" style="display: none" data-id="{$cus.id}"></a>
                    <a id="albumsAddx" onclick="$('#albumsAdd input').click();" style="z-index: 999" class="fixAddBtn" href="javascript:;"> </a>
                    <div class="list">
                        <a id="uploadComplete" href="#albumAddFrame"></a>
                        <div id="albumAddFrame">
                            <img class="img" />
                            <div class="tArea">
                                <textarea id="albumDesc" placeholder="Say Something"></textarea>
                            </div>
                            <div class="textAlignCenter">
                                <a id="albumAddComfirm" href="javascript:;" class="wd-btn primary">Done</a>
                            </div>
                        </div>
                        <div class="Thead"><span>Photos</span></div>
                        <div class="clearfix" id="albumList">
                            {include file='./profile_albums.tpl'}
                        </div>
                    </div>
                </div>

                <a id="goViewAlbum" class="ghBtn" href="?/vAlbum/ls/id={$cus.id}">View Photos ({$cus.stat.album})</a>

            </div>
        </div>
        {*        <script type="text/javascript">
        $(function () {
        var container = document.querySelector('#ALBUM');
        window.msnry = new Masonry(container, {
        itemSelector: '.albums',
        gutter: ".gutter-sizer"
        });
        $('.albums img').on('load', function () {
        msnry.layout();
        });
        });
        </script>*}
    </body>
</html>