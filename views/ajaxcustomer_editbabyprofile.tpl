<form id="editProfile">
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">姓名</div>
            <div class="gs-text">
                <input type="text" name="cus_name" value="{if $ed}{$cus.cus_name}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">性别</div>
            <div class="gs-text">
                <input type="text" name="cus_sex" value="{if $ed}{$cus.cus_sex}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">年龄</div>
            <div class="gs-text">
                <input type="text" name="cus_age" value="{if $ed}{$cus.cus_age}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">生日</div>
            <div class="gs-text">
                <input type="text" name="cus_birth" value="{if $ed}{$cus.cus_birth}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">电子邮箱</div>
            <div class="gs-text">
                <input type="text" name="cus_email" value="{if $ed}{$cus.cus_email}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">联系电话</div>
            <div class="gs-text">
                <input type="text" name="cus_phone" value="{if $ed}{$cus.cus_phone}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
    </div>
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">省</div>
            <div class="gs-text">
                <input type="text" name="cus_prov" value="{if $ed}{$cus.cus_prov}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">市</div>
            <div class="gs-text">
                <input type="text" name="cus_city" value="{if $ed}{$cus.cus_city}{/if}" id="pd-form-title" autofocus/>
            </div>
        </div>
    </div>
    <div class="gs-label">详细地址</div>
    <div class="gs-text">
        <input type="text" name="cus_addr" value="{if $ed}{$cus.cus_addr}{/if}" id="pd-form-title" autofocus/>
    </div>
    <div class="gs-label">身份证号</div>
    <div class="gs-text">
        <input type="text" name="cus_personid" value="{if $ed}{$cus.cus_personid}{/if}" id="pd-form-title" autofocus/>
    </div>
    <div class="textAlignCenter">
        <a id="addJournalBtn" onclick="editProfileClick()" href="javascript:;" class="wd-btn primary">Done</a>
    </div>
</form>