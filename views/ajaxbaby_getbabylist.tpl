{if $cus.babys}{foreach from=$cus.babys item=baby}
        <div class='Babys' id='baby-{$baby.id}'>
            <div class="edit hidden">
                <a class="addbaby fancybox.ajax" data-fancybox-type="ajax" href="?/ajaxBaby/editBabyProfile/cid={$cus.id}">Add</a>
                <a class="editbaby fancybox.ajax" data-fancybox-type="ajax" href="?/ajaxBaby/editBabyProfile/bid={$baby.id}">Edit</a>
                <a class='deletebaby' href='javascript:;' onclick='fnDeleteBaby({$baby.id})'>Delete</a>
            </div>
            <a class="jUploadBaby" data-id='{$baby.id}' href="javascript:;"><img class="babyhead" src="{if $baby.b_head neq ''}{$baby.b_head}{else}layouts/Thumbnail/?p=/upload/{$album.gpath}&h=80&w=80/layouts/images/profle_1.png{/if}" /></a>
            <div class='bProfiles'>
                <div style='width: 140px;float:left;'>
                    <p style='font-size: 13px;'><em>姓名：</em>{$baby.b_name}</p>
                    <p><em>性别：</em>{$baby.b_sex}</p>
                    <p><em>年龄：</em>{$baby.b_age}岁</p>
                    <p><em>生日：</em>{$baby.b_birth}</p>
                    <p><em>距生日：</em><span style="color:#44b549">{$baby.birthDist}</span>天</p>
                </div>
                <div style='margin-left: 140px;padding-right: 15px;'>
                    <p><em>备注：</em>{$baby.b_remark}</p>
                </div>
            </div>
        </div>
{/foreach}{else}
    <div class="babyListEmpty">
        请&nbsp;<a class="addbaby fancybox.ajax" data-fancybox-type="ajax" href="?/ajaxBaby/editBabyProfile/cid={$cus.id}">点击</a>&nbsp;添加宝宝信息
    </div>
{/if}