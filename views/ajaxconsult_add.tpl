<form id="editProfile">
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">姓名</div>
            <div class="gs-text">
                <input type="text" value="" id="pd-consult-name" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">性别</div>
            <select id="pd-consult-sex">
                <option value="男">男</option>
                <option value="女">女</option>
            </select>
        </div>
    </div>
    <div class="clearfix">
        <div style="float:left;width: 48%">
            <div class="gs-label">电子邮箱</div>
            <div class="gs-text">
                <input type="text" value="" id="pd-consult-email" autofocus/>
            </div>
        </div>
        <div style="float:right;width: 48%">
            <div class="gs-label">联系电话</div>
            <div class="gs-text">
                <input type="text" value="" id="pd-consult-phone" autofocus/>
            </div>
        </div>
    </div>
    初始密码为 <b>hm2015</b>
    <div class="textAlignCenter">
        <a id="addConsultBtn" href="javascript:;" class="wd-btn primary">Done</a>
    </div>
</form>