<!DOCTYPE html>
<html>
    <head>
        <title> </title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/home.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/fancyBox/source/jquery.fancybox.css" type="text/css" rel="Stylesheet" />
        <link href="layouts/jss/umeditor/themes/default/css/umeditor.min.css" type="text/css" rel="Stylesheet" />
        <script data-main="layouts/jss/inc/homeindex.js?v={$smarty.now}" src="layouts/jss/require.min.js"></script>
    </head>
    <body style="overflow: hidden">
        <input type="hidden" id="cusid" value="{$cus.id}" />
         {foreach from=$list item=cus}
            <div class="card" data-id="{$cus.id}" id="card{$cus.id}">
                <div class="journal" id="journal-{$cus.id}}">
                    <ul class="jul">
                        {foreach from=$cus.journal item=js}
                            <li><span>[{$js.jdate}]</span> {$js.jcont}</li>
                            {/foreach}
                    </ul>
                    <a class="addjournal fancybox.ajax" data-fancybox-type="ajax" href="layouts/static/addJournal.html">Write Journal</a>
                </div>
                <div class="cusinfo">
                    <div class="cusleft">
                        <a class="jUpload" href="javascript:;" data-id="{$cus.id}"><img class="cushead" src="{if $cus.cus_head neq ''}{$cus.cus_head}{else}layout/images/profle_1.png{/if}" /></a>
                        <div class="profile">
                            <div class='pl'>{include file='./ajaxcustomer_getcustomerinfo.tpl'}</div>
                            <div class="edit">
                                <a class="addProfile fancybox.ajax hidden" data-fancybox-type="ajax" href="?/ajaxCustomer/editProfile/">Edit</a>&nbsp;
                                <a class="editProfile fancybox.ajax" data-fancybox-type="ajax" href="?/ajaxCustomer/editProfile/id={$cus.id}">Edit</a>&nbsp;
                                <a class='delete' href='javascript:;' onclick='fnDeleteCustomer({$cus.id})'>Delete</a>
                            </div>
                        </div>
                    </div>
                    <div class="cusright">
                        <div class="cusbabys">
                            {include file='./ajaxbaby_getbabylist.tpl'}
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}
    </body>
</html>