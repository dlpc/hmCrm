<!DOCTYPE html>
<html>
    <head>
        <title>CRM后台管理登录</title>
        <meta charset="utf-8" />
        <meta name="renderer" content="webkit">
        <link href="./favicon.ico" rel="Shortcut Icon" />
        <link href="layouts/css/login.css?v={$cssversion}" type="text/css" rel="Stylesheet" />
        <script data-main="layouts/jss/inc/loginindex.js?v={$smarty.now}" src="layouts/jss/require.min.js"></script>
    </head>
    <body class="loginBody" style="background-image:url('layouts/images/backgrounds/{$rand}.jpg');">
        <div id="welcome"></div>
        <div id="login" class="clearfix">
            <div id='loginWrap'></div>
            <form class="login-form" id="login-frame" action="/pages/login_validate.php" method="POST">
                <img src="layouts/images/getheadimg.jpg" height="100px" width="100px" />
                <p> &nbsp; </p>
                <div class='login-item'>           
                    <div class="gs-text">
                        <input type="text" tabindex="1" value="{$smarty.cookies.admin_acc}" name="username" id="pd-form-username" placeholder="用户名"/>
                    </div>
                </div>
                <div class='login-item'>     
                    <div class="gs-text">
                        <input type="password" tabindex="2" name="password" id="pd-form-password" placeholder="密码" />
                    </div>
                </div>
                <div class='login-item'>  
                    <a class="login-gbtn" href="javascript:;">登录</a>
                </div>
                <div id="copyrights" style="color:#777;opacity: 0.8;font-size:12px;margin-top: 10px;">&COPY; 2014-2015 iWshop All rights reserved.</div>
            </form>
        </div>
    </body>
</html>