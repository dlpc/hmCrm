/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50528
Source Host           : 127.0.0.1:3306
Source Database       : hmcrm

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2015-04-02 08:17:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hmcrm_album
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_album`;
CREATE TABLE `hmcrm_album` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `gname` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gpath` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_album
-- ----------------------------
INSERT INTO `hmcrm_album` VALUES ('16', '17', '', '14274364905514f3ca077d0.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('17', '17', '', '14274369755514f5af132b8.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('18', '17', '', '14274369795514f5b38d77a.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('19', '17', '', '14274369875514f5bb5c4ec.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('20', '17', '', '14274370045514f5cc98c97.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('21', '17', '', '14274379035514f94f57fc3.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('22', '17', '', '14274391935514fe5984dc6.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('23', '13', '', '14274392145514fe6e85ac0.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('24', '13', '', '14274392255514fe7970ef9.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('25', '17', '', '1427440047551501afd77ac.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('26', '17', '', '142744240155150ae1e6ed9.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('27', '17', '', '142744280155150c71e2a09.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('28', '17', '', '142744300555150d3de33c3.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('29', '17', '', '142744301155150d43037f3.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('30', '17', '', '142744302355150d4f037f8.jpg', '2015-03-27');
INSERT INTO `hmcrm_album` VALUES ('31', '17', '', '14277155965519360c917ee.jpg', '2015-03-30');
INSERT INTO `hmcrm_album` VALUES ('32', '17', '', '1427715600551936101b258.jpg', '2015-03-30');
INSERT INTO `hmcrm_album` VALUES ('33', '17', '', '142771560255193612de64b.jpg', '2015-03-30');
INSERT INTO `hmcrm_album` VALUES ('34', '17', '', '142771560955193619df460.jpg', '2015-03-30');
INSERT INTO `hmcrm_album` VALUES ('35', '17', '', '14277156155519361fcb3e8.jpg', '2015-03-30');
INSERT INTO `hmcrm_album` VALUES ('36', '17', '', '14277612465519e85ef00b1.jpg', '2015-03-31');
INSERT INTO `hmcrm_album` VALUES ('37', '8', '', '1427797143551a7497a0d0e.jpg', '2015-03-31');
INSERT INTO `hmcrm_album` VALUES ('38', '8', '', '1427797146551a749a1d843.jpg', '2015-03-31');
INSERT INTO `hmcrm_album` VALUES ('39', '8', '', '1427797148551a749cca3a2.jpg', '2015-03-31');
INSERT INTO `hmcrm_album` VALUES ('40', '8', '', '1427797152551a74a06ec81.jpg', '2015-03-31');

-- ----------------------------
-- Table structure for hmcrm_babys
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_babys`;
CREATE TABLE `hmcrm_babys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `b_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `b_birth` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `b_sex` enum('男','女') COLLATE utf8_bin DEFAULT '男',
  `b_age` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `b_remark` text COLLATE utf8_bin,
  `b_head` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hmcrm_babys
-- ----------------------------
INSERT INTO `hmcrm_babys` VALUES ('14', '50', '1', '1', '', '1', 0x31, 'upload/142721289355118a5d81528.jpg');
INSERT INTO `hmcrm_babys` VALUES ('15', '50', '2', '2', '', '2', 0x32, 'upload/142721293655118a886998c.jpg');
INSERT INTO `hmcrm_babys` VALUES ('16', '49', '1', '1', '', '1', 0x31, 'upload/142721297155118aab7b38b.jpg');
INSERT INTO `hmcrm_babys` VALUES ('17', '14', '123', '3123', '', '21312', 0x3132336B6F696A6F, 'upload/142721315555118b639da04.jpg');
INSERT INTO `hmcrm_babys` VALUES ('18', '13', '222', '222', '', '22', 0x3232, 'upload/14274392195514fe732458c.jpg');
INSERT INTO `hmcrm_babys` VALUES ('19', '13', '22', '1', '', '1', 0x31, 'upload/14274392225514fe760d9c2.jpg');
INSERT INTO `hmcrm_babys` VALUES ('20', '0', '123', '1', '', '3', 0x3233, null);
INSERT INTO `hmcrm_babys` VALUES ('21', '0', '12', '2', '', '23', 0x31, null);
INSERT INTO `hmcrm_babys` VALUES ('22', '0', '123', '2', '', '123', 0x32, null);
INSERT INTO `hmcrm_babys` VALUES ('23', '0', '2', '2', '', '1', 0x31, null);
INSERT INTO `hmcrm_babys` VALUES ('24', '0', '123', '1', '', '2', 0x32, null);
INSERT INTO `hmcrm_babys` VALUES ('25', '0', '12', '2', '', '2', 0x32, null);
INSERT INTO `hmcrm_babys` VALUES ('26', '16', '123', '213', '', '1', 0x313233, 'upload/1427798525551a79fd203d4.jpg');
INSERT INTO `hmcrm_babys` VALUES ('30', '17', '123addd', '123123213', '', '2', 0x5648532076656E69616D20616C69717561207468756E6465726361747320646F6C6F7265206B65666669796568, 'upload/14277017725519000cd4511.jpg');
INSERT INTO `hmcrm_babys` VALUES ('32', '17', 'asd', 'asd', '', '123123', 0x617364617364617364, null);

-- ----------------------------
-- Table structure for hmcrm_consultant
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_consultant`;
CREATE TABLE `hmcrm_consultant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `pwd` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hmcrm_consultant
-- ----------------------------
INSERT INTO `hmcrm_consultant` VALUES ('1', 'ycchen', '79ba4ba9b03ec8e8204912fcb5ea50ad7a8d6df52fed4e61f60102270005cdfd1fbb43a6d36daaae22c0cf52cf5ab1a0', '2015-03-19 09:55:12', 'koodo@qq.com');

-- ----------------------------
-- Table structure for hmcrm_customers
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_customers`;
CREATE TABLE `hmcrm_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cus_name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_addate` date DEFAULT NULL,
  `cus_age` tinyint(4) DEFAULT NULL,
  `cus_sex` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_uid` tinyint(4) DEFAULT '0',
  `cus_head` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_birth` date DEFAULT NULL,
  `cus_city` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_prov` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_addr` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_personid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `cus_phone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hmcrm_customers
-- ----------------------------
INSERT INTO `hmcrm_customers` VALUES ('4', '陈永才2', '2015-03-20', '22', '男', '1', 'upload/14273534025513af3ad790e.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('5', '陈永才', '2015-03-20', '22', '男', '1', 'upload/550d1724738ab.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('6', '陈永才', '2015-03-20', '22', '男', '1', 'upload/550d1a2d2a902.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('7', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('8', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('9', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('10', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('11', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('12', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('13', '陈永才', '2015-03-20', '22', '男', '1', 'upload/14272802785512919673ce1.jpg', '1992-12-12', '广州', '广东', '广州市大学城中环西路100号', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('15', 'j', '2015-03-25', '0', '男', '0', null, '0000-00-00', 'j', 'j', 'j', 'j', 'j', 'j');
INSERT INTO `hmcrm_customers` VALUES ('16', '123', '2015-03-26', '12', '男', '1', 'upload/1427798527551a79ffa24c6.jpg', '0000-00-00', '123', '123', '123', '123', '123', '123');
INSERT INTO `hmcrm_customers` VALUES ('17', '黄国凯', '2015-03-26', '12', '男', '1', 'upload/1427933673551c89e97f148.jpg', '1992-12-02', '广州', '广东2', 'Four loko artisan viral', '440823199212126512', 'koodo@qq.com', '18565518404');
INSERT INTO `hmcrm_customers` VALUES ('18', '1231', '2015-03-31', '127', '男', '1', 'upload/1427933664551c89e0b9c91.jpg', '0000-00-00', '3123', '12', '123', '123', '312', '3123');
INSERT INTO `hmcrm_customers` VALUES ('19', '1231x', '2015-04-02', '12', '男', '1', null, '1992-12-12', '123', '123', '123', '123', '123', '123');
INSERT INTO `hmcrm_customers` VALUES ('20', '1231x', '2015-04-02', '12', '男', '1', null, '1992-12-12', '123', '123', '123', '123', '123', '123');
INSERT INTO `hmcrm_customers` VALUES ('21', '2131', '2015-04-02', '23', '男', '1', null, '1992-01-12', '123', '123', '123', '', '3123', '123');

-- ----------------------------
-- Table structure for hmcrm_customer_labelhash
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_customer_labelhash`;
CREATE TABLE `hmcrm_customer_labelhash` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hashkey` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `label_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_customer_labelhash
-- ----------------------------

-- ----------------------------
-- Table structure for hmcrm_customer_labels
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_customer_labels`;
CREATE TABLE `hmcrm_customer_labels` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) unsigned DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_customer_labels
-- ----------------------------

-- ----------------------------
-- Table structure for hmcrm_ext
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_ext`;
CREATE TABLE `hmcrm_ext` (
  `id` int(11) NOT NULL,
  `ext` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hmcrm_ext
-- ----------------------------

-- ----------------------------
-- Table structure for hmcrm_ideas
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_ideas`;
CREATE TABLE `hmcrm_ideas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `idea` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_ideas
-- ----------------------------
INSERT INTO `hmcrm_ideas` VALUES ('1', '17', 'Four loko artisan viral', null);
INSERT INTO `hmcrm_ideas` VALUES ('2', '17', '2', null);
INSERT INTO `hmcrm_ideas` VALUES ('3', '17', '2131231', null);
INSERT INTO `hmcrm_ideas` VALUES ('4', '17', '123213', null);
INSERT INTO `hmcrm_ideas` VALUES ('5', '17', '123123', '2015-03-31 18:10:50');
INSERT INTO `hmcrm_ideas` VALUES ('6', '17', '123213', '2015-03-31 18:14:48');
INSERT INTO `hmcrm_ideas` VALUES ('7', '8', '123123', '2015-03-31 18:18:47');
INSERT INTO `hmcrm_ideas` VALUES ('8', '17', 'sdsdsd', '2015-03-31 18:26:00');
INSERT INTO `hmcrm_ideas` VALUES ('9', '17', 'asdasdasd', '2015-03-31 18:40:47');
INSERT INTO `hmcrm_ideas` VALUES ('10', '17', 'VHS veniam aliqua thundercats dolore keffiyeh', '2015-03-31 18:40:52');
INSERT INTO `hmcrm_ideas` VALUES ('11', '16', 'asdaxasxasdasd', '2015-03-31 18:41:59');

-- ----------------------------
-- Table structure for hmcrm_journal
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_journal`;
CREATE TABLE `hmcrm_journal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(4) DEFAULT NULL,
  `jtitle` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `jdate` datetime DEFAULT NULL,
  `jcont` text COLLATE utf8_bin,
  `cusid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of hmcrm_journal
-- ----------------------------
INSERT INTO `hmcrm_journal` VALUES ('1', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3F3F3A3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('2', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('3', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('4', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('5', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('6', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('7', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('8', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('9', '50', 'adasdas', '2015-03-20 11:55:50', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '0');
INSERT INTO `hmcrm_journal` VALUES ('17', '50', '', '2015-03-20 22:45:47', 0x313233313233, '0');
INSERT INTO `hmcrm_journal` VALUES ('21', '50', '', '2015-03-20 22:53:54', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('22', '50', '', '2015-03-20 22:54:08', 0x6265616E7374616C6B203F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F2E2E2E, '1');
INSERT INTO `hmcrm_journal` VALUES ('23', '50', '', '2015-03-21 08:18:24', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('24', '50', '', '2015-03-21 08:22:47', 0x313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('25', '50', '', '2015-03-21 08:22:55', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('26', '50', '', '2015-03-21 08:23:08', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('27', '50', '', '2015-03-21 08:23:47', 0x787878, '1');
INSERT INTO `hmcrm_journal` VALUES ('28', '50', '', '2015-03-21 08:24:12', 0x78617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('29', '50', '', '2015-03-21 08:24:26', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('30', '50', '', '2015-03-21 08:25:09', 0x646173, '1');
INSERT INTO `hmcrm_journal` VALUES ('31', '50', '', '2015-03-21 08:25:13', 0x617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('32', '50', '', '2015-03-21 08:25:35', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('33', '50', '', '2015-03-21 08:25:45', 0x61736461736478617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('34', '50', '', '2015-03-21 08:26:01', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('35', '50', '', '2015-03-21 08:26:05', 0x78787878, '1');
INSERT INTO `hmcrm_journal` VALUES ('36', '50', '', '2015-03-21 08:26:08', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('37', '50', '', '2015-03-21 08:26:15', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('38', '50', '', '2015-03-21 08:26:15', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('39', '50', '', '2015-03-21 08:26:15', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('40', '50', '', '2015-03-21 08:26:25', 0x617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('41', '50', '', '2015-03-21 08:27:41', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('42', '50', '', '2015-03-21 08:27:51', 0x313233313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('43', '50', '', '2015-03-21 08:28:09', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('44', '50', '', '2015-03-21 08:43:57', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('45', '50', '', '2015-03-21 08:44:03', 0x78787878787878787878787878, '1');
INSERT INTO `hmcrm_journal` VALUES ('46', '50', '', '2015-03-21 09:03:28', 0x6265616E7374616C6B203F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F3F2E2E2E, '1');
INSERT INTO `hmcrm_journal` VALUES ('47', '50', '', '2015-03-21 09:29:18', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '1');
INSERT INTO `hmcrm_journal` VALUES ('48', '50', '', '2015-03-21 09:29:31', 0x3F3F3F3A3F3F3F3F3F3F3F3F3F3F203F3F3F3F3F3F3F, '1');
INSERT INTO `hmcrm_journal` VALUES ('49', '50', '', '2015-03-21 10:54:59', 0x3233343234323334, '1');
INSERT INTO `hmcrm_journal` VALUES ('50', '50', '', '2015-03-21 11:24:00', 0x3834363534363534, '1');
INSERT INTO `hmcrm_journal` VALUES ('51', '50', '', '2015-03-21 13:39:53', 0x313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('52', '51', '', '2015-03-23 15:14:08', 0x6B6A6B6A6B6A, '1');
INSERT INTO `hmcrm_journal` VALUES ('53', '14', '', '2015-03-25 09:48:48', 0x313233313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('54', '14', '', '2015-03-25 14:04:05', 0x3F3F3F3F3F3F3F3F3F3F3F3F, '1');
INSERT INTO `hmcrm_journal` VALUES ('55', '14', '', '2015-03-25 14:47:59', 0x6B6F62677568766776, '1');
INSERT INTO `hmcrm_journal` VALUES ('56', '14', '', '2015-03-25 16:38:31', 0x313233313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('57', '14', '', '2015-03-25 16:38:57', 0x6B6F6F646F, '1');
INSERT INTO `hmcrm_journal` VALUES ('58', '13', '', '2015-03-25 21:39:20', 0x69696969, '1');
INSERT INTO `hmcrm_journal` VALUES ('59', '16', '', '2015-03-26 22:58:21', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('61', '17', '', '2015-03-27 15:38:29', 0x5648532076656E69616D20616C69717561207468756E6465726361747320646F6C6F7265206B656666697965682C206469726563742074726164652061757468656E7469632050425220656975736D6F642E20446F6C6F72652076656E69616D2066616E6E79207061636B2C2070726F6964656E74206970686F6E65206D697874617065206972757265207365642E204C6F2D6669206C656767696E6773207374756D70746F776E20636172646967616E20657473792C207069746368666F726B206563686F207061726B2074657272792072696368617264736F6E206E657874206C6576656C206C61626F72652070726F6964656E742E, '1');
INSERT INTO `hmcrm_journal` VALUES ('62', '17', '', '2015-03-27 15:51:59', 0x53656D696F74696373206D6C6B73686B20616E696D207375737461696E61626C6520627574636865722074656D706F722E, '1');
INSERT INTO `hmcrm_journal` VALUES ('71', '17', '', '2015-03-30 15:23:37', 0x323133313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('72', '17', '', '2015-03-31 18:34:08', 0x7165717765717765, '1');
INSERT INTO `hmcrm_journal` VALUES ('73', '17', '', '2015-03-31 18:35:17', 0x313233323133, '1');
INSERT INTO `hmcrm_journal` VALUES ('74', '17', '', '2015-03-31 18:35:30', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('75', '17', '', '2015-03-31 18:35:35', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('76', '17', '', '2015-03-31 18:36:22', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('77', '17', '', '2015-03-31 18:37:01', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('78', '17', '', '2015-03-31 18:37:05', 0x617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('79', '17', '', '2015-03-31 18:37:18', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('80', '17', '', '2015-03-31 18:37:57', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('81', '17', '', '2015-03-31 18:38:03', 0x313233313233, '1');
INSERT INTO `hmcrm_journal` VALUES ('82', '17', '', '2015-03-31 18:38:29', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('83', '17', '', '2015-03-31 18:38:44', 0x3132333231617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('84', '17', '', '2015-03-31 18:38:58', 0x617364617364617364, '1');
INSERT INTO `hmcrm_journal` VALUES ('85', '17', '', '2015-03-31 19:13:59', 0x5648532076656E69616D20616C69717561207468756E6465726361747320646F6C6F7265206B656666697965682C206469726563742074726164652061757468656E7469632050425220656975736D6F642E20446F6C6F72652076656E69616D2066616E6E79207061636B2C2070726F6964656E74206970686F6E65206D697874617065206972757265207365642E204C6F2D6669206C656767696E6773207374756D70746F776E20636172646967616E20657473792C207069746368666F726B206563686F207061726B2074657272792072696368617264736F6E206E657874206C6576656C206C61626F72652070726F6964656E742E, '1');
INSERT INTO `hmcrm_journal` VALUES ('86', '17', '', '2015-04-01 12:34:19', 0x617364617364, '1');

-- ----------------------------
-- Table structure for hmcrm_newfeeds
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_newfeeds`;
CREATE TABLE `hmcrm_newfeeds` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `ftype` int(11) DEFAULT NULL,
  `fhash` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `ftime` datetime DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_newfeeds
-- ----------------------------

-- ----------------------------
-- Table structure for hmcrm_remind
-- ----------------------------
DROP TABLE IF EXISTS `hmcrm_remind`;
CREATE TABLE `hmcrm_remind` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT NULL,
  `remind` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hmcrm_remind
-- ----------------------------
INSERT INTO `hmcrm_remind` VALUES ('1', '17', 'VHS veniam aliqua thundercats dolore keffiyeh', null);
INSERT INTO `hmcrm_remind` VALUES ('2', '17', '2', null);
INSERT INTO `hmcrm_remind` VALUES ('3', '17', 'opopo', null);
INSERT INTO `hmcrm_remind` VALUES ('4', '17', '2', null);
INSERT INTO `hmcrm_remind` VALUES ('5', '17', '123123123', null);
INSERT INTO `hmcrm_remind` VALUES ('6', '17', '123123', '2015-03-31 18:14:44');
INSERT INTO `hmcrm_remind` VALUES ('7', '8', '123123asdasd', '2015-03-31 18:18:51');
INSERT INTO `hmcrm_remind` VALUES ('8', '8', '广州市大学城中环西路100号', '2015-03-31 18:18:56');
INSERT INTO `hmcrm_remind` VALUES ('9', '17', '22223', '2015-03-31 18:25:57');
INSERT INTO `hmcrm_remind` VALUES ('10', '16', 'asdasd            margin-bottom: 0;\n', '2015-03-31 18:41:55');
INSERT INTO `hmcrm_remind` VALUES ('11', '17', 'asdas', '2015-04-01 12:34:14');
